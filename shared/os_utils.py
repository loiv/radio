import os
import pathlib

from shared.config import Config


def check_path_exists(path: str) -> bool:
    return os.path.exists(path)


def create_path_if_not_exists(tracks_path: str) -> None:
    if not check_path_exists(tracks_path):
        os.mkdir(tracks_path)


def get_paths_or_create_track_files(downloaded, converted) -> tuple[str, str]:
    current_file_path = os.path.abspath(__file__)
    module_file_path = pathlib.Path(os.path.dirname(current_file_path))
    project_file_path = module_file_path.parent.absolute()

    downloaded_tracks_path = os.path.join(
        project_file_path, Config.DIRECTORY_DOWNLOAD_NAME
    )
    ready_to_play_tracks_path = os.path.join(
        project_file_path, Config.DIRECTORY_READY_TO_PLAY_NAME
    )

    create_path_if_not_exists(downloaded_tracks_path)
    create_path_if_not_exists(ready_to_play_tracks_path)

    downloaded_file_path = os.path.join(downloaded_tracks_path, downloaded)
    converted_file_path = os.path.join(ready_to_play_tracks_path, converted)

    return (downloaded_file_path, converted_file_path)
