from datetime import datetime, time, timedelta
from typing import Optional

from shared.config import Config
from shared.custom_types import Seconds
from shared.database.values import get_breaks, get_time_shift


def get_break_start_time(asked_datetime: datetime) -> Optional[time]:
    if asked_datetime.tzinfo is None:
        raise ValueError("No timezone specified!")
    breaks = get_breaks()
    for break_start_time in breaks:
        break_start_datetime = datetime.combine(
            datetime.now(tz=Config.OUR_TIMEZONE).date(),
            break_start_time,
            tzinfo=Config.OUR_TIMEZONE,
        )
        break_end_datetime = break_start_datetime + timedelta(
            seconds=get_break_duration(break_start_time)
        )
        if break_start_datetime <= asked_datetime <= break_end_datetime:
            return break_start_time
    return None


def get_current_break_start_time() -> Optional[time]:
    local_datetime = datetime.now(tz=Config.OUR_TIMEZONE) + timedelta(
        seconds=get_time_shift()
    )
    return get_break_start_time(local_datetime)


def get_remaining_time_to_next_break() -> Seconds:
    breaks = get_breaks()
    time_shift = get_time_shift()
    for break_start_time in breaks:
        break_start_datetime = datetime.combine(
            datetime.now(tz=Config.OUR_TIMEZONE).date(),
            break_start_time,
            tzinfo=Config.OUR_TIMEZONE,
        )
        local_time_including_bell_delay = datetime.now(
            tz=Config.OUR_TIMEZONE
        ) + timedelta(seconds=time_shift)

        if local_time_including_bell_delay <= break_start_datetime:
            diff = break_start_datetime - local_time_including_bell_delay
            return Seconds(int(diff.total_seconds()))

    now_datetime = datetime.now(tz=Config.OUR_TIMEZONE)
    tomorrow_datetime = now_datetime + timedelta(days=1)
    first_break_time = list(breaks)[0]
    first_break_tomorrow = datetime.combine(
        tomorrow_datetime.date(),
        first_break_time,
        tzinfo=Config.OUR_TIMEZONE,
    )
    diff = first_break_tomorrow - now_datetime
    return Seconds(int(diff.total_seconds()))


def get_break_duration(starts_at: time) -> Seconds:
    breaks = get_breaks()
    return Seconds(breaks[starts_at])


def get_seconds_left_during_current_break() -> Seconds:
    break_start = get_current_break_start_time()
    if break_start is None:
        return Seconds(0)

    break_start_datetime = datetime.combine(
        datetime.now(tz=Config.OUR_TIMEZONE).date(),
        break_start,
        tzinfo=Config.OUR_TIMEZONE,
    )
    return get_seconds_left_during_break(break_start_datetime)


def get_seconds_left_during_break(break_start_datetime: datetime) -> Seconds:
    break_start_time = break_start_datetime.astimezone(
        tz=Config.OUR_TIMEZONE
    ).time()
    break_duration = get_break_duration(break_start_time)
    break_end_datetime = break_start_datetime + timedelta(
        seconds=break_duration
    )

    left_time = break_end_datetime - datetime.now(tz=Config.OUR_TIMEZONE)
    seconds_left = int(left_time.total_seconds())

    if seconds_left <= 0:
        return Seconds(0)
    elif seconds_left > break_duration:
        return Seconds(break_duration)
    return Seconds(seconds_left)


# Bell delay shift doesn't applicable - this refers to scheduling tracks
def check_publication_time_is_over(asked_datetime: datetime) -> bool:
    breaks = get_breaks()
    break_start_time = asked_datetime.astimezone(Config.OUR_TIMEZONE).time()
    break_duration = breaks[break_start_time]

    break_end_time = asked_datetime + timedelta(seconds=break_duration)
    diff = break_end_time - datetime.now(tz=Config.OUR_TIMEZONE)

    if int(diff.total_seconds()) >= 0:
        return False
    return True
