from typing import Final


class TrackErrorMessages:
    UNKNOWN_PROVIDER: Final = (
        "Incompatible track url - we do not support this music provider"
    )
    NO_TRACK_ID: Final = "No track id"
    NO_TRACK_URL: Final = "No track url"
    INVALID_YOUTUBE_TRACK_URL: Final = (
        "Incorrect Youtube track url - cannot extract track id"
    )
