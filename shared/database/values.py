from shared.custom_types import Breaks, Seconds
from shared.database.connection import db
from shared.database.setup_utils import load_breaks_info, load_time_shift

_BREAKS = None
_TIME_SHIFT = None


def get_breaks() -> Breaks:
    global _BREAKS

    if _BREAKS:
        return _BREAKS
    _BREAKS = load_breaks_info(db)
    return _BREAKS


def get_time_shift() -> Seconds:
    global _TIME_SHIFT

    if _TIME_SHIFT is not None:
        return Seconds(_TIME_SHIFT)

    _TIME_SHIFT = load_time_shift(db)
    return Seconds(_TIME_SHIFT)
