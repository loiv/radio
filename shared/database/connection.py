from .connect_utils import connect_to_database

client = connect_to_database()
db = client["radio"]

library = db["library"]
queue = db["queue"]
history = db["history"]
waiting = db["waiting"]
users = db["users"]
status = db["status"]
certs = db["certs"]
breaks_config = db["breaksConfig"]
