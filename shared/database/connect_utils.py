import pymongo
from shared.config import Config, get_logger

logger = get_logger(__name__)


def log_connection_status(client: pymongo.MongoClient) -> None:
    db_command_result = client["radio"].command("connectionStatus")
    logger.info(
        f"Connected to the database - {client.address} - "
        + f"{db_command_result['authInfo']}"
    )


def _prepare_connection_string() -> str:
    USER = Config.MONGO_USER
    PASSWORD = Config.MONGO_PASSWORD

    conn = "mongodb://"
    if USER and PASSWORD:
        conn += f"{USER}:{PASSWORD}@"

    if not USER:
        logger.warning("No MongoDB user specified!")
    if not PASSWORD:
        logger.warning("No MongoDB password specified!")

    conn += f"{Config.MONGO_HOST}:{Config.MONGO_PORT}/radio"
    return conn


def connect_to_database(auth_source="radio") -> pymongo.MongoClient:
    connection_string = _prepare_connection_string()
    client = pymongo.MongoClient(
        connection_string,
        directConnection=True,
        authSource=auth_source,
        serverSelectionTimeoutMS=5000,
        connect=True,
        readPreference="primary",
    )
    log_connection_status(client)
    return client
