from datetime import datetime, timedelta
from typing import Any, Optional, OrderedDict, TypedDict, TypeGuard
from zoneinfo import ZoneInfo

from pymongo.collection import Collection
from shared.config import Config, get_logger
from shared.custom_types import (
    DB_PLAYER_QUERY,
    NoTrackPlaying,
    PlayerStatusData,
    PlayerStatusUpdate,
    Seconds,
    TrackPlaying,
    TrackUrl,
)
from shared.database.connection import history, library, queue, status, waiting
from shared.time_utils import get_break_duration, get_current_break_start_time

logger = get_logger(__name__)


def update_player_status(
    track_url: Optional[TrackUrl | NoTrackPlaying] = None,
    manually_stopped: Optional[bool] = None,
    error_occured: Optional[bool] = None,
    during_break: Optional[bool] = None,
) -> bool:

    query_fields: PlayerStatusUpdate = {}
    if manually_stopped is not None:
        query_fields["manuallyStopped"] = manually_stopped
    if error_occured is not None:
        query_fields["errorOccured"] = error_occured
    if track_url is not None:
        query_fields["trackUrl"] = track_url
    if during_break is not None:
        query_fields["duringBreak"] = during_break

    set_query = {"$set": query_fields}

    result = status.update_one(DB_PLAYER_QUERY, set_query, upsert=True)
    if result.modified_count == 1:
        logger.info(f"Updated music player status – set {query_fields}")
    else:
        logger.debug("Called database update but not modified")
    return bool(result.modified_count)


def move_waiting_tracks_to_queue(track_url: TrackUrl) -> None:
    pipeline = [
        {
            "$match": {
                "trackUrl": track_url,
                "datetime": {"$gte": datetime.now()},
            }
        },
        {"$sort": {"weight": 1}},
        {
            "$lookup": {
                "from": "queue",
                "localField": "datetime",
                "foreignField": "datetime",
                "as": "queuedAtSameDt",
            }
        },
        {"$addFields": {"queuedTracksCount": {"$size": "$queuedAtSameDt"}}},
        {
            "$match": {
                "queuedTracksCount": {
                    "$lte": Config.MAX_TRACKS_QUEUED_ONE_BREAK
                }
            }
        },
        {
            "$lookup": {
                "from": "library",
                "localField": "queuedAtSameDt.trackUrl",
                "foreignField": "trackUrl",
                "as": "libraryData",
            }
        },
        {"$addFields": {"totalDuration": {"$sum": "$libraryData.duration"}}},
        {
            "$addFields": {
                "breakStartTime": {
                    "$dateToString": {
                        "date": "$datetime",
                        "format": "%H:%M",
                        "timezone": Config.OUR_TIMEZONE.key,
                    }
                }
            }
        },
        {
            "$lookup": {
                "from": "breaksConfig",
                "localField": "breakStartTime",
                "foreignField": "startTime",
                "as": "break",
            }
        },
        {"$addFields": {"breakInfo": {"$first": "$break"}}},
        {
            "$match": {
                "$expr": {
                    "$lt": [
                        "$totalDuration",
                        "$breakInfo.duration",
                    ]
                }
            }
        },
        {
            "$addFields": {
                "highestQueueWeight": {"$max": "$queuedAtSameDt.weight"}
            }
        },
        {
            "$set": {
                "weight": {"$add": [{"$max": ["$highestQueueWeight", 0]}, 1]}
            }
        },
        {
            "$project": {
                "_id": 1,
                "datetime": 1,
                "trackUrl": 1,
                "weight": 1,
            }
        },
        {
            "$merge": {
                "into": "queue",
            }
        },
    ]

    _ = waiting.aggregate(pipeline)

    logger.info(f"Moved tracks {track_url} from waiting list to queue.")
    deleted_count = waiting.delete_one({"trackUrl": track_url}).deleted_count
    logger.info(
        f"Deleted {deleted_count} tracks from waiting as they were merged"
        + " or they are outdated"
    )


def get_tracks_list(
    track_statuses: list[str],
    offset: int = 0,
    downloaded: Optional[bool] = None,
    search: Optional[str] = None,
) -> dict[str, str | list[str]]:
    QueryConditions = TypedDict(
        "QueryConditions",
        {
            "status": dict[str, list[str]],
            "downloaded": bool,
            "$text": dict[str, str],
        },
        total=False,
    )

    pagination = 20
    topWaitingDatetimesCount = 6
    return_fields: dict[str, Any] = {
        "title": 1,
        "trackUrl": 1,
        "duration": 1,
        "status": 1,
        "_id": 0,
    }

    sort = OrderedDict({"title": 1})
    match_conditions: QueryConditions = {"status": {"$in": track_statuses}}

    if downloaded is not None:
        match_conditions["downloaded"] = downloaded
    if search is not None:
        match_conditions["$text"] = {"$search": search}
        sort = OrderedDict({"score": {"$meta": "textScore"}}, **sort)

    query = [
        {"$match": match_conditions},
        {"$sort": sort},
        {"$skip": pagination * (offset or 0)},
        {"$limit": pagination},
        {
            "$lookup": {
                "from": "waiting",
                "localField": "trackUrl",
                "foreignField": "trackUrl",
                "as": "dtObjArray",
            }
        },
        {"$addFields": {"waitingDatetimes": "$dtObjArray.datetime"}},
        {
            "$project": {
                **return_fields,
                "waitingDatetimesFiltered": {
                    "$filter": {
                        "input": "$waitingDatetimes",
                        "as": "datetime",
                        "cond": {"$gt": ["$$datetime", datetime.now()]},
                    }
                },
            }
        },
        {
            "$project": {
                **return_fields,
                "waitingDatetimesSorted": {
                    "$sortArray": {
                        "input": "$waitingDatetimesFiltered",
                        "sortBy": 1,
                    }
                },
            }
        },
        {
            "$project": {
                **return_fields,
                "topWaitingDatetimes": {
                    "$slice": [
                        "$waitingDatetimesSorted",
                        topWaitingDatetimesCount,
                    ]
                },
            }
        },
    ]

    cursor = library.aggregate(query)
    tracks_list = list(cursor)
    all_count = int(library.count_documents(match_conditions))
    has_next_page = bool((pagination * (offset + 1)) < all_count)
    response = {
        "success": True,
        "count": all_count,
        "tracks": tracks_list,
        "hasNext": has_next_page,
    }
    return response


def get_track_queue_or_history_search_query(
    start_day: datetime, end_day: Optional[datetime] = None
) -> list[dict]:
    if end_day is None:
        end_day = start_day + timedelta(days=1)
    # We do reply with JSON array which - in terms of RFC7159 - keeps the order
    # so we sort scheduled tracks by its playing time and weight then

    # query limit is not neccessary due to behaviour -
    # a *finite*, not exceeding ~30 of tracks are able to be played a day
    # Furthermore, we should send a full list of played tracks
    return [
        {"$match": {"datetime": {"$gte": start_day, "$lte": end_day}}},
        {
            "$lookup": {
                "from": "library",
                "localField": "trackUrl",
                "foreignField": "trackUrl",
                "as": "helpfield",
            }
        },
        {
            "$unwind": {
                "path": "$helpfield",
                "preserveNullAndEmptyArrays": False,
            }
        },
        {
            "$addFields": {
                "duration": "$helpfield.duration",
                "title": "$helpfield.title",
            }
        },
        {"$sort": {"datetime": 1, "weight": 1}},
        {"$project": {"helpfield": 0, "_id": 0, "weight": 0}},
    ]


def get_break_queued_tracks_count(publish_datetime: datetime) -> int:
    search = {
        "datetime": {
            "$gte": publish_datetime,
            "$lte": publish_datetime + timedelta(minutes=1),
        },
    }
    return int(queue.count_documents(search))


def remove_rejected_tracks_from_queue(track_url: TrackUrl) -> int:
    result = queue.delete_many({"trackUrl": track_url})
    return result.deleted_count


def get_queue_weight_at(publish_datetime: datetime) -> int:
    pipeline = [
        {"$match": {"datetime": publish_datetime}},
        {"$group": {"_id": "datetime", "maxWeight": {"$max": "$weight"}}},
        {"$project": {"_id": 0, "maxWeight": 1}},
    ]
    result = list(queue.aggregate(pipeline))
    count = result[0]["maxWeight"] if len(result) > 0 else 0
    try:
        weight = int(count)
    except ValueError:
        logger.warning(f"Got {count} as weight from database")
        return 0
    else:
        return weight


def _get_track_to_play_at(
    playing_datetime: datetime,
) -> Optional[dict]:
    query = {
        "datetime": {
            "$gte": playing_datetime,
            "$lte": playing_datetime + timedelta(minutes=1),
        },
    }
    result = queue.find_one(
        query,
        sort=[("weight", 1)],
        projection={"trackUrl": 1, "datetime": 1, "_id": 0},
    )
    if result is not None and "trackUrl" in result:
        return result
    return None


def get_track_to_play() -> Optional[TrackPlaying]:
    current_break_start_time = get_current_break_start_time()
    if not current_break_start_time:
        return None
    track_to_play_datetime = datetime.combine(
        datetime.now().date(),
        current_break_start_time,
        tzinfo=Config.OUR_TIMEZONE,
    )
    query_result = _get_track_to_play_at(track_to_play_datetime)
    if not query_result:
        return None

    track_playing_datetime: datetime = query_result["datetime"]
    track_to_play = TrackPlaying(
        query_result["trackUrl"],
        track_playing_datetime.replace(tzinfo=ZoneInfo("UTC")),
    )
    return track_to_play


def get_tracks_duration_from(
    collection: Collection, publish_datetime: datetime
) -> Seconds:
    match_conditions = {
        "datetime": {
            "$gte": publish_datetime,
            "$lte": publish_datetime + timedelta(minutes=1),
        }
    }
    query = [
        {"$match": match_conditions},
        {
            "$lookup": {
                "from": "library",
                "localField": "trackUrl",
                "foreignField": "trackUrl",
                "as": "libraryTrack",
            }
        },
        {"$addFields": {"info": {"$arrayElemAt": ["$libraryTrack", 0]}}},
        {
            "$group": {
                "_id": {"datetime": "$datetime"},
                "totalDuration": {"$sum": "$info.duration"},
            }
        },
    ]
    cursor = collection.aggregate(query)
    result = list(cursor)

    total_duration = result[0]["totalDuration"] if len(result) > 0 else 0
    return Seconds(total_duration)


def get_left_playing_time_during_break(playing_datetime: datetime) -> Seconds:
    break_start_time = playing_datetime.astimezone(
        tz=Config.OUR_TIMEZONE
    ).time()
    break_duration = get_break_duration(break_start_time)
    queued_tracks_total_duration = get_tracks_duration_from(
        queue, playing_datetime
    )
    played_tracks_total_duration = get_tracks_duration_from(
        history, playing_datetime
    )
    total_tracks_duration = (
        queued_tracks_total_duration + played_tracks_total_duration
    )
    return Seconds(max(break_duration - total_tracks_duration, 0))


def is_valid_player_status(data: dict) -> TypeGuard[PlayerStatusData]:
    required_keys = PlayerStatusData.__required_keys__  # type: ignore
    return isinstance(data, dict) and all(
        key in data for key in list(required_keys)
    )


def get_player_status_data() -> Optional[PlayerStatusData]:
    projection = {"type": 0, "_id": 0}
    assert status.count_documents(DB_PLAYER_QUERY) <= 1
    result = status.find_one(DB_PLAYER_QUERY, projection=projection)
    if is_valid_player_status(result):
        return result
    return None
