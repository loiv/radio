from datetime import time

import pymongo
from shared.config import get_logger
from shared.custom_types import Breaks, DatabaseBreak, Seconds, TrackUrl

logger = get_logger(__name__)


def create_text_search_index(db: pymongo.database.Database) -> None:
    # Calling this method multiple times do not cause any problem - see
    # https://docs.mongodb.com/manual/reference/method/
    # db.collection.createIndex/#recreating-an-existing-index
    db.library.create_index([("title", "text")])


def create_unique_indexes(db: pymongo.database.Database) -> None:
    db.library.create_index([("trackUrl", 1)], unique=True)
    db.users.create_index([("identificator", 1)], unique=True)
    db.status.create_index([("type", 1)], unique=True)
    db.certs.create_index([("kid", 1)], unique=True)
    db.certs.create_index([("expiresAt", 1)], expireAfterSeconds=0)


def create_music_player_status_keys(db: pymongo.database.Database) -> None:
    insert_query = {
        "type": "player",
        "errorOccured": False,
        "duringBreak": False,
        "manuallyStopped": True,
        "trackUrl": "",
    }
    db.status.insert_one(insert_query)


def insert_breaks_start_times(db: pymongo.database.Database):
    # key = starting time, value = duration in seconds
    breaks = Breaks(
        {
            time(8, 30): Seconds(10 * 60),
            time(9, 25): Seconds(10 * 60),
            time(10, 20): Seconds(10 * 60),
            time(11, 15): Seconds(15 * 60),
            time(12, 15): Seconds(10 * 60),
            time(13, 10): Seconds(10 * 60),
            time(14, 5): Seconds(10 * 60),
            time(15, 00): Seconds(10 * 60),
        }
    )

    breaks_db: list[DatabaseBreak] = []
    for t, dur in breaks.items():
        break_: DatabaseBreak = {
            "type": "breakStartTime",
            "startTime": t.strftime("%H:%M"),
            "duration": dur,
        }
        breaks_db.append(break_)

    db["breaksConfig"].insert_many(breaks_db)


def insert_time_shift(db: pymongo.database.Database):
    time_shift = {"type": "timeShift", "timeShift": 0}
    db["breaksConfig"].insert_one(time_shift)


def load_breaks_info(db: pymongo.database.Database) -> Breaks:
    breaks = Breaks({})
    founds_breaks: list[DatabaseBreak] = db["breaksConfig"].find(
        {"type": "breakStartTime"}
    )
    for found_break in founds_breaks:
        break_start_time = time.fromisoformat(found_break["startTime"])
        duration = Seconds(int(found_break["duration"]))
        breaks[break_start_time] = duration
    logger.debug(f"Loaded and set {breaks=}")
    return breaks


def load_time_shift(db: pymongo.database.Database) -> Seconds:
    found_time_shift = db["breaksConfig"].find_one({"type": "timeShift"})
    time_shift = found_time_shift["timeShift"]
    logger.debug(f"Loaded and set {time_shift=}")
    return Seconds(time_shift)


def create_radio_app_role(db: pymongo.database.Database) -> None:
    db.command(
        "createRole",
        "radioApplication",
        privileges=[
            {
                "resource": {"db": "radio", "collection": "users"},
                "actions": ["find", "insert", "update", "remove"],
            },
            {
                "resource": {"db": "radio", "collection": "history"},
                "actions": ["find", "insert"],
            },
            {
                "resource": {"db": "radio", "collection": "waiting"},
                "actions": ["find", "insert", "remove"],
            },
            {
                "resource": {"db": "radio", "collection": "library"},
                "actions": ["find", "insert", "update"],
            },
            {
                "resource": {"db": "radio", "collection": "queue"},
                "actions": ["find", "insert", "update", "remove"],
            },
            {
                "resource": {"db": "radio", "collection": "certs"},
                "actions": ["find", "insert", "update", "remove"],
            },
            {
                "resource": {"db": "radio", "collection": "status"},
                "actions": ["find", "insert", "update", "changeStream"],
            },
            {
                "resource": {"db": "radio", "collection": "breaksConfig"},
                "actions": ["find", "update"],
            },
        ],
        roles=[],
    )


def initialize_database(db: pymongo.database.Database):
    from shared.database.query_utils import update_player_status

    create_text_search_index(db)
    create_unique_indexes(db)
    create_music_player_status_keys(db)
    create_radio_app_role(db)
    insert_breaks_start_times(db)
    insert_time_shift(db)

    update_player_status(
        track_url=TrackUrl(""),
        manually_stopped=True,
        error_occured=False,
    )
