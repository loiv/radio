import abc
import string
import urllib.parse as urlparse
from datetime import datetime, timedelta
from json import JSONDecodeError
from typing import Optional
from urllib.parse import parse_qs, unquote

from shared.config import Config, TrackLibraryStatuses, get_logger
from shared.custom_types import (
    Seconds,
    TrackAPIConfigurationError,
    TrackAPIConnectionError,
    TrackAPIMalformedResponse,
    TrackAPINoResults,
    TrackAPITimeout,
    TrackId,
    TrackIdentifierError,
    TrackLibraryUnknownStatus,
    TrackStatusUpdateError,
    TrackUrl,
)
from shared.messages import TrackErrorMessages
from shared.os_utils import get_paths_or_create_track_files

logger = get_logger(__name__)


def parse_isoduration(isostring: str) -> dict[str, int]:
    separators = {
        "D": "days",
        "H": "hours",
        "M": "minutes",
        "S": "seconds",
    }
    assert isostring[:2] == "PT"
    isostring = isostring[2:]

    duration_vals = {}
    for sep, unit in separators.items():
        partitioned = isostring.partition(sep)
        if partitioned[1] == sep:
            isostring = partitioned[2]
            duration = int(partitioned[0])
            duration_vals.update({unit: duration})
        else:
            duration_vals.update({unit: 0})
    return duration_vals


def check_channel_belongs_official_artist(channel_id: str) -> Optional[bool]:
    import requests

    headers = {
        "X-YouTube-Client-Name": "1",
        "X-YouTube-Client-Version": "2.20201021.03.00",
        "Accept-Language": "en-US,en;q=0.9",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/"
        + "537.36 (KHTML, like Gecko) Chrome/105.0.5195.102 Safari/537.36",
    }

    internal_api_required_suffix = "channels?flow=grid&view=0&pbj=1"
    yt_channel_url = f"https://www.youtube.com/channel/{channel_id}/"
    url = yt_channel_url + internal_api_required_suffix

    response = requests.get(url, headers=headers)
    try:
        parsed = response.json()
    except requests.exceptions.JSONDecodeError:
        logger.warning(
            f"Endpoint {url} with headers {headers}"
            + " returned JSON-unparsable response"
        )
        return None

    try:
        header_rendered = parsed[1]["response"]["header"][
            "c4TabbedHeaderRenderer"
        ]
    except (IndexError, KeyError) as ex:
        logger.warning(f"Response does not contain expected keys - {ex}")
        return None

    if "badges" not in header_rendered:
        return False

    try:
        badge = header_rendered["badges"][0]
        badge_type = badge["metadataBadgeRenderer"]["icon"]["iconType"]
    except (IndexError, KeyError) as ex:
        logger.warning(f"Expected valid badge metadata - {ex}")
        return None
    else:
        return badge_type == "OFFICIAL_ARTIST_BADGE"


class Track(abc.ABC):
    file_prefix = ""

    def __init__(self, track_id: TrackId, track_url: TrackUrl) -> None:
        self._id = track_id
        self._url = track_url
        self._duration: Seconds = Seconds(0)
        self._title: str = ""

    @abc.abstractmethod
    def get_duration(self) -> Seconds:
        pass

    @abc.abstractmethod
    def get_title(self) -> str:
        pass

    @property
    def id(self) -> TrackId:
        if self._id:
            return self._id
        raise TrackIdentifierError(TrackErrorMessages.NO_TRACK_ID)

    @property
    def url(self) -> TrackUrl:
        if self._url:
            return self._url
        raise TrackIdentifierError(TrackErrorMessages.NO_TRACK_URL)

    def _get_data_from_database(self) -> Optional[dict]:
        from shared.database.connection import library

        result = library.find_one({"trackUrl": self._url})
        return result

    @property
    def status(self) -> TrackLibraryStatuses:
        data = self._get_data_from_database()
        if data is None:
            return TrackLibraryStatuses.NOT_IN_LIBRARY

        database_status = data["status"]
        posibble_statuses = {
            status.value: TrackLibraryStatuses[status.name]
            for status in TrackLibraryStatuses
        }
        try:
            status = posibble_statuses[database_status]
        except KeyError:
            raise TrackLibraryUnknownStatus(
                f"Invalid status of track {repr(self)} "
                + f"in database: {database_status}"
            )
        else:
            return status

    @property
    def is_downloaded(self) -> bool:
        data = self._get_data_from_database()
        if data is None or not data["downloaded"]:
            return False
        return True

    @staticmethod
    def match_provider(track_url: TrackUrl) -> "YoutubeTrack":
        if isinstance(track_url, bytes):
            track_url = TrackUrl(track_url.decode())
        if not isinstance(track_url, str):
            raise TypeError(f"Passed {type(track_url)} type, expected 'str'")

        if not track_url.startswith("http"):
            track_url = TrackUrl("https://" + track_url)
        parsed_url = urlparse.urlparse(track_url)

        if parsed_url.netloc in [
            "youtube.com",
            "m.youtube.com",
            "www.youtube.com",
            "youtu.be",
        ]:
            return YoutubeTrack(track_url)
        else:
            raise TrackIdentifierError(TrackErrorMessages.UNKNOWN_PROVIDER)

    def get_paths(self) -> tuple[str, str]:
        downloaded_filename = f"{self.file_prefix}_{self._id}"
        converted_filename = f"{self.file_prefix}_{self._id}.wav"
        file_paths = get_paths_or_create_track_files(
            downloaded_filename, converted_filename
        )
        return file_paths

    def mark_as_downloaded(self) -> int:
        from shared.database.connection import library

        result = library.update_one(
            {"trackUrl": self._url},
            {"$set": {"downloaded": True}},
        )
        return int(result.modified_count)

    def unmark_as_downloaded(self) -> int:
        from shared.database.connection import library

        result = library.update_one(
            {"trackUrl": self._url},
            {"$set": {"downloaded": False}},
        )
        return int(result.modified_count)

    def mark_manual_download_needed(self) -> int:
        from shared.database.connection import library

        result = library.update_one(
            {"trackUrl": self._url},
            {"$set": {"needsManualDownload": True}},
        )
        return int(result.modified_count)

    def delete_from_queue_at(self, publish_datetime: datetime) -> int:
        from shared.database.connection import queue

        deleting_query = {
            "trackUrl": self._url,
            "datetime": {
                "$gte": publish_datetime,
                "$lte": publish_datetime + timedelta(minutes=1),
            },
        }
        deleted_count = queue.delete_one(deleting_query).deleted_count
        return int(deleted_count)

    def check_played_or_queued_at(self, publish_datetime: datetime) -> int:
        from shared.database.connection import history, queue

        search = {
            "trackUrl": self._url,
            "datetime": {
                "$gte": publish_datetime.replace(
                    hour=0, minute=0, second=0, microsecond=0
                ),
                "$lte": publish_datetime.replace(
                    hour=23, minute=59, second=59, microsecond=999
                ),
            },
        }
        total_count = int(
            history.count_documents(search) + queue.count_documents(search)
        )
        return total_count

    def check_waiting_at(self, publish_datetime: datetime) -> bool:
        from shared.database.connection import waiting

        search = {
            "trackUrl": self._url,
            "datetime": {
                "$gte": publish_datetime.replace(
                    hour=0, minute=0, second=0, microsecond=0
                ),
                "$lte": publish_datetime.replace(
                    hour=23, minute=59, second=59, microsecond=999
                ),
            },
        }
        count = int(waiting.count_documents(search))
        return bool(count)

    def add_to_queue_at(self, publish_datetime: datetime) -> None:
        from shared.database.connection import queue
        from shared.database.query_utils import get_queue_weight_at

        queue.insert_one(
            {
                "datetime": publish_datetime,
                "trackUrl": self._url,
                "weight": get_queue_weight_at(publish_datetime) + 1,
            }
        )

    def add_to_library_as_pending_to_approve(self) -> None:
        from shared.database.connection import library

        library.insert_one(
            {
                "trackUrl": self._url,
                "duration": self.get_duration(),
                "title": self.get_title(),
                "status": TrackLibraryStatuses.PENDING_APPROVAL.value,
                "downloaded": False,
            }
        )

    def add_as_waiting_with_datetime_to_play(
        self, publish_datetime: datetime
    ) -> None:
        from shared.database.connection import waiting
        from shared.database.query_utils import get_queue_weight_at

        waiting.insert_one(
            {
                "datetime": publish_datetime,
                "trackUrl": self._url,
                "weight": get_queue_weight_at(publish_datetime) + 1,
            }
        )
        logger.debug(f"Added {self} to waiting list at {publish_datetime}")

    def update_status(self, new_status: TrackLibraryStatuses) -> None:
        from shared.database.connection import library

        result = library.update_one(
            {"trackUrl": self._url}, {"$set": {"status": new_status.value}}
        )
        if result.matched_count == 1:
            logger.info(f"Updated track {self} status - set {new_status}")
        else:
            raise TrackStatusUpdateError(
                f"No track (with url: {self._url}) matched "
                + f" in an update command. Desired status: {new_status}"
            )

    def check_valid_duration(self) -> bool:
        return self.get_duration() <= Config.MAX_TRACK_DURATION_SECONDS

    def add_to_playing_history_at(self, play_datetime: datetime) -> None:
        from shared.database.connection import history

        played_track = {
            "trackUrl": self._url,
            "datetime": play_datetime,
        }
        history.insert_one(played_track)

    def _check_track_duration_in_database(self) -> Optional[int]:
        result = self._get_data_from_database()
        if result and (duration := result.get("duration")):
            return int(duration)
        return None

    def _check_track_title_in_database(self) -> Optional[str]:
        result = self._get_data_from_database()
        if result and (title := result.get("title")):
            return title
        return None


class YoutubeTrack(Track):
    file_prefix = "youtube"

    def __init__(self, url: TrackUrl) -> None:
        identifier = TrackId(self._get_youtube_track_id(url))
        url = TrackUrl(YoutubeTrack._build_standard_track_url(url))
        super().__init__(identifier, url)
        self._id = identifier
        self._url = url

    def __str__(self) -> str:
        return f'YoutubeTrack("{self._url}")'

    def __repr__(self) -> str:
        return (
            f"<YoutubeTrack id={self._id} url={self._url} "
            + f"{self._duration=} {self._title=}>"
        )

    def __eq__(self, other) -> bool:
        if isinstance(other, YoutubeTrack):
            return self._url == other.url
        return False

    @staticmethod
    def _build_standard_track_url(track_url: str) -> TrackUrl:
        base_url = "https://www.youtube.com/watch?"
        track_id = YoutubeTrack._get_youtube_track_id(track_url)
        desired_parameters = {"v": track_id}
        urlencoded_parameters = urlparse.urlencode(desired_parameters)
        standard_track_url = base_url + urlencoded_parameters
        return TrackUrl(standard_track_url)

    @staticmethod
    def _check_if_track_id_really_valid(track_id: str) -> bool:
        track_id_charset = [*string.ascii_letters, *string.digits, "-", "_"]
        return len(track_id) == 11 and all(
            character in track_id_charset for character in track_id
        )

    @staticmethod
    def _get_youtube_track_id(track_url: str) -> str:
        if YoutubeTrack._check_if_track_id_really_valid(track_url):
            track_id = track_url
            return track_id
        if not track_url.startswith("http"):
            track_url = TrackUrl("https://" + track_url)
        unquoted = unquote(track_url)
        parsed_url = urlparse.urlparse(unquoted)
        if (
            parsed_url.netloc
            in ["youtube.com", "m.youtube.com", "www.youtube.com"]
            and parsed_url.path == "/watch"
        ):
            v_param = parse_qs(parsed_url.query).get("v")
            if (
                v_param
                and len(v_param) > 0
                and YoutubeTrack._check_if_track_id_really_valid(v_param[0])
            ):
                track_id = v_param[0]
                return track_id
        elif parsed_url.netloc == "youtu.be":
            track_id = TrackId(parsed_url.path[1:])
            if YoutubeTrack._check_if_track_id_really_valid(track_id):
                return track_id
        raise TrackIdentifierError(
            TrackErrorMessages.INVALID_YOUTUBE_TRACK_URL
        )

    def _get_youtube_api_data(self, part: str) -> dict:
        import requests

        api_url = (
            f"{Config.YOUTUBE_API_URL}?part={part}&id={self.id}"
            f"&key={Config.YOUTUBE_API_KEY}"
        )
        try:
            resp = requests.get(api_url, timeout=(1, 3))
        except requests.exceptions.ConnectionError:
            raise TrackAPIConnectionError(f"Unable to connect to {api_url}")
        except requests.Timeout:
            raise TrackAPITimeout(
                f"Timeout occured during request to Youtube API "
                f"to fetch {part} part of id={self.id}"
            )
        try:
            json_response = resp.json()
        except JSONDecodeError:
            raise TrackAPIMalformedResponse(
                "An error occured during decoding response to request "
                + f"of Youtube's id={self.id} in {part} part"
            )

        # even if KeyError is thrown (so the response's JSON is malformed)
        # and has (not) been handled,
        # the server won't finally respond with anything other than HTTP 500
        if (
            "error" in json_response
            and "key not valid" in json_response["error"]["message"]
        ):
            message = "Passed Youtube API key not valid!"
            logger.error(
                message + " Verify its correctness in the configuration!"
            )
            raise TrackAPIConfigurationError(message)

        results_count = json_response["pageInfo"]["totalResults"]
        if results_count == 0:
            raise TrackAPINoResults("Youtube API returns 0 tracks' info")

        requested_part_info = json_response["items"][0][part]
        return requested_part_info

    def get_duration(self) -> Seconds:
        if self._duration:
            return self._duration

        looked_up_db_duration = self._check_track_duration_in_database()
        if looked_up_db_duration is not None:
            self._duration = Seconds(looked_up_db_duration)
            return self._duration

        api_response = self._get_youtube_api_data("contentDetails")
        iso_duration = api_response["duration"]
        assert isinstance(iso_duration, str)

        parsed_time = parse_isoduration(iso_duration)
        td = timedelta(**parsed_time)
        duration = Seconds(int(td.total_seconds()))
        self._duration = duration
        return self._duration

    def get_title(self) -> str:
        if self._title:
            return self._title

        looked_up_db_title = self._check_track_title_in_database()
        if looked_up_db_title is not None:
            self._title = looked_up_db_title
            return looked_up_db_title

        api_response = self._get_youtube_api_data("snippet")
        track_title = api_response["title"]
        self._title = track_title

        delims = ["-", "–", "—", "|"]
        topic_suffix = "- Topic"
        if not any([ch in track_title for ch in delims]):
            channel_title: str = api_response["channelTitle"]
            channel_id: str = api_response["channelId"]
            if check_channel_belongs_official_artist(channel_id):
                author = channel_title
                full_title = f"{author} - {track_title}"
                self._title = full_title
            elif topic_suffix in channel_title:
                author = channel_title.removesuffix(topic_suffix).strip()
                full_title = f"{author} - {track_title}"
                self._title = full_title

        return self._title
