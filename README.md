# Radio

## School-related project implementing an automated, user-friendly radio station

### Features

* add your favourite songs to a daily queue
* specify the playing time during the day
* provide API for getting and managing tracks list
* automatically download and play accepted tracks
* store a requested playing time and put it into the queue when the track gets accepted
* provide buttons to manually stop/start playing

Also validate:

* single track's duration
* total tracks duration scheduled on one break
* if specified date or time is not over (or during a weekend)
* if a requested track have been scheduled at that day

### Install

1. Clone this repository

```bash
git clone https://gitlab.com/loiv/radio.git
```

2. Create and activate the python virtual environment

```bash
cd radio
python3 -m venv venv
source venv/bin/activate
```

3. Install dependencies

```bash
pip install wheel
pip install -r radio/requirements.txt
pip install -r notifier/requirements.txt
pip install -r music/requirements.txt
```

### Run

1. Run MongoDB database and specify its port in the `settings.toml` file

2. Type

```bash
flask run
```

or - on production - use [Gunicorn](https://gunicorn.org/):

```python
gunicorn --bind 127.0.0.1:5777:5777 --certfile /path/to/server.crt --keyfile /path/to/server.key --access-logfile - --error-logfile - radio_app:app
```

See _Dockerfile.dev_ files at _music/_ and _notifier/_ to see how to run them on production.

### Secrets

Both Gunicorn (due to at least 2 workers) and multiple Docker containers need to have **same** environment variables, especially `SECRET_KEY`. Unless given, you will have trouble with correct cookies signing.

`SECRET_KEY` needs to be [CSPRNG](https://en.wikipedia.org/wiki/Cryptographically-secure_pseudorandom_number_generator).\
Therefore, use e.g

```bash
python -c 'from os import urandom; from base64 import b64encode; print(b64encode(urandom(32)).decode("utf-8"))'
```

and pass it as `SECRET_KEY` .

_Note that is **recommended** to use e.g. Docker **secrets** with Docker-Compose to handle sensitive values such a `SECRET_KEY` ._
Use Docker file secrets to pass them or specify them in the `.secrets.toml` file or pass as environment variables.

`YOUTUBE_API_KEY` can be generated [here](https://console.cloud.google.com/apis/).\
`CAPTCHA_SECRET_KEY` can be generated [here](https://friendlycaptcha.com/signup/).

### Running tests

Before testing, run (once):

```bash
pip install -e .
python tests/prepare.py
```

and then

```bash
pytest
```

### Music service

`python3-dev` and `libasound2-dev` needs to be installed:

``` bash
sudo apt-get install -y python3-dev libasound2-dev
```

#### You can play a track manually

1. Downloading and converting it:

``` python
from music.utils import prepare_music_file
prepare_music_file('https://www.youtube.com/watch?v=EtHVDF3gbaA')
```

2. Play the track:

``` python
import asyncio
from music import start_playing_track

seconds = 15
track_file_path = "gotowe/youtube_EtHVDF3gbaA.wav"

async def main(): 
    start_playing_track(track_file_path, seconds)
    await asyncio.sleep(seconds)


asyncio.run(main())
```

### Database

First time run:

```bash
>>> from shared.database.setup_utils import initialize_database
>>> initialize_database()
```

and then:

```javascript
db.createUser({
    user: "radioUser",
    pwd: passwordPrompt(),
    customData: {
        userId: 0
    },
    roles: [{
        role: "radioApplication",
        db: "radio"
    }],
}, )
```

in the Mongo Shell.

To initiate the ReplicaSet, run

```javascript
rs.initiate({_id : "rs0", members: [{ _id: 0, host: "radio_mongodb" }]})
```

---
Also create a radio administrator (Google ID needed):

```bash
>>> from radio.auth.models import User
>>> user = User(identificator='...', name='...', email='...')
>>> user.save_to_database()
```

---
At the end restrict access to the MongoDB - add

```yml
security:
   authorization: enabled
```

to the `/etc/mongo.conf` or an other configuration file.

### Hints for developers

1. As you **must verify** JWT token's audience (to check whether it matches to your [Google API client ID](https://developers.google.com/identity/gsi/web/guides/get-google-api-clientid)), specify the identifier.\
You can find the latter [here](https://console.cloud.google.com/apis/credentials/) - click on the appriopriate project and copy the `Client ID`.\
Insert them as a list (`GSI_CLIENTS_IDS` at the `[api]` section in `settings.toml`).

2. Pass Google ID as `identificator`.\
Go [here](https://developers.google.com/people/api/rest/v1/people/get), fill a _resourceName_ field with `people/me`, _personFields_ with `names` and execute the query.\
`dictionary["names"][0]["metadata"]["source"]["id"]` should be your ID.

3. You can encounter CORS error while developing frontend app locally.\
This can be helpful - at the `create_app` function in `__init__.py`:

    ``` python
    from flask_cors import CORS
    CORS(app, origins=["http://localhost:3000"], supports_credentials=True)
    ```

4. You can verify if music is playing by e.g. `pacmd list-sink-inputs`

5. Run containers with Docker Compose - `TIME_TO_SPOOF="@2022-04-18 06:29:50" docker-compose -f docker-compose.yml -f docker-compose.dev.yml up`
