import asyncio
from datetime import datetime, time, timedelta
from unittest.mock import patch

import pytest
from freezegun import freeze_time
from music.player import play_track, until_music_should_play
from tests.conftest import PointlessWaiting, TestConfig

TIME_SHIFT = 0

break_start_time = time(9, 25, tzinfo=TestConfig.OUR_TIMEZONE)
timestamp_during_break = datetime.combine(
    datetime.now(tz=TestConfig.OUR_TIMEZONE).date(),
    break_start_time,
    tzinfo=TestConfig.OUR_TIMEZONE,
) + timedelta(seconds=TIME_SHIFT)

timestamp_between_breaks = timestamp_during_break + timedelta(minutes=23)


@patch("shared.time_utils.get_time_shift", return_value=TIME_SHIFT)
@patch("music.player.asyncio.sleep")
@patch("music.player.create_task_to_mark_track_as_played")
@patch("music.player.update_player_status")
@patch("music.player.start_playing_track")
@patch("music.player.get_seconds_left_during_current_break")
@patch("shared.track.Track._check_track_duration_in_database", return_value=2)
@patch("music.player.check_path_exists", return_value=True)
@patch("shared.database.query_utils._get_track_to_play_at")
@patch("music.player.until_music_should_play")
def test_playing_track_when_it_should(
    mocked_playing_condition,
    mocked_get_track_to_play,
    mocked_path_existence,
    mocked_track_look_up_duration,
    mocked_playing_duration_get,
    mocked_start_playing_track,
    mocked_updating_player_status_in_db,
    mocked_marking_track_as_played_task,
    mocked_sleeping,
    mocked_time_shift,
):
    track_id = "I5xZRlQFYj0"
    track_url = f"https://www.youtube.com/watch?v={track_id}"
    mocked_get_track_to_play.return_value = {
        "trackUrl": track_url,
        "datetime": datetime.now(),
    }

    mocked_playing_condition.return_value = True

    track_playing_duration = 2
    mocked_playing_duration_get.return_value = track_playing_duration

    mocked_sleeping.side_effect = PointlessWaiting("No point in waiting")

    with freeze_time(timestamp_during_break), pytest.raises(PointlessWaiting):
        asyncio.run(play_track())

    mocked_start_playing_track.assert_called()
    (
        arg_track_path,
        arg_playing_duration,
    ) = mocked_start_playing_track.call_args.args
    assert track_id in arg_track_path
    assert arg_playing_duration == track_playing_duration

    mocked_updating_player_status_in_db.assert_called()
    mocked_marking_track_as_played_task.assert_called()

    mocked_path_existence.assert_called()
    mocked_playing_duration_get.assert_called()


@patch("music.player.wave.open")
@patch("music.player.asyncio.sleep")
def test_stop_music_if_break_is_up(mocked_sleep, mocked_play_music):
    mocked_sleep.side_effect = PointlessWaiting("No point in waiting")

    with freeze_time(timestamp_between_breaks), pytest.raises(
        PointlessWaiting
    ):
        asyncio.run(play_track())

    mocked_sleep.assert_called()

    mocked_play_music.assert_not_called()


@patch("music.player.asyncio.sleep")
def test_wait_a_moment_unless_music_should_play(mocked_sleeping):
    mocked_sleeping.side_effect = PointlessWaiting(
        "I'll sleep - music shouldn't be played (yet)"
    )

    with patch("music.player.check_if_manually_stopped", return_value=True):
        with pytest.raises(PointlessWaiting):
            asyncio.run(until_music_should_play())

    with patch(
        "music.player.check_if_manually_stopped", return_value=False
    ), patch("music.player.get_current_break_start_time", return_value=None):
        with pytest.raises(PointlessWaiting):
            asyncio.run(until_music_should_play())
