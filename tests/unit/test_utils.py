from typing import Final
from unittest.mock import patch

import pytest
import requests
from pytest import mark
from shared.custom_types import TrackUrl
from shared.track import Track, TrackIdentifierError
from tests.conftest import TestConfig

expected: Final = "i-uYn_lp6Qg"


@mark.parametrize(
    "track_url",
    [
        f"https://www.youtube.com/watch?v={expected}",
        f"www.youtube.com/watch?v={expected}",
        f"youtube.com/watch?v={expected}",
        f"https://youtu.be/{expected}",
        f"https://m.youtube.com/watch%3Fv%3D{expected}",
        (
            f"https://www.youtube.com/watch?v={expected}"
            + "&list=RDCMUCTEopVgqNCUhJq57CxTc4aw"
            + "&start_radio=1&rv=YZo9Am1t9qw&t=4"
        ),
    ],
)
def test_get_track_id_full_url(track_url):
    track = Track.match_provider(track_url)
    assert track.id == expected


def test_raising_error_when_trying_to_pass_invalid_url():
    invalid_urls = [
        "pqrUQrAcfo4",
        "https://www.google.com/url?sa=t&"
        + "url=https://m.youtube.com/watch%3Fv%3DpqrUQrAcfo4",
    ]
    for url in invalid_urls:
        with pytest.raises(TrackIdentifierError):
            _ = Track.match_provider(url)  # type: ignore


@patch("shared.track.Track._check_track_duration_in_database")
def test_get_track_duration_from_cache(mocked_db_lookup):
    mocked_db_lookup.return_value = 196
    track_url = TrackUrl("https://www.youtube.com/watch?v=UbYQErtM9Zk")
    track = Track.match_provider(track_url)
    duration = track.get_duration()
    assert duration == 196


@TestConfig.my_vcr.use_cassette()
@patch("shared.track.Track._check_track_duration_in_database")
def test_get_track_duration_from_api(mocked_db_lookup):
    mocked_db_lookup.return_value = None
    track_url = TrackUrl("https://www.youtube.com/watch?v=UbYQErtM9Zk")
    track = Track.match_provider(track_url)
    duration = track.get_duration()
    assert duration == 196


@mark.parametrize(
    "api_duration, expected_parsed_duration",
    [("PT2S", 2), ("PT3M16S", 3 * 60 + 16)],
)
@patch("shared.track.Track._check_track_duration_in_database")
@patch.object(requests, "get")
def test_properly_parse_track_duration_api_response(
    mock_request_get, mocked_db_lookup, api_duration, expected_parsed_duration
):
    track_url = TrackUrl("https://www.youtube.com/watch?v=i-uYnZlp6Qg")
    track = Track.match_provider(track_url)
    response_data_json = {
        "kind": "youtube#videoListResponse",
        "etag": "...",
        "items": [
            {
                "kind": "youtube#video",
                "etag": "...",
                "id": "i-uYnZlp6Qg",
                "contentDetails": {
                    "duration": api_duration,
                    "dimension": "2d",
                    "definition": "hd",
                    "caption": "false",
                    "licensedContent": False,
                    "contentRating": {},
                    "projection": "rectangular",
                },
            }
        ],
        "pageInfo": {"totalResults": 1, "resultsPerPage": 1},
    }

    def res():
        r = requests.Response()
        r.status_code = 200

        def json_func():
            return response_data_json

        r.json = json_func  # type: ignore
        return r

    mock_request_get.return_value = res()
    mocked_db_lookup.return_value = None
    assert track.get_duration() == expected_parsed_duration


@TestConfig.my_vcr.use_cassette()
@patch("shared.track.Track._check_track_title_in_database", return_value=None)
def test_get_track_title(mock_title):
    track_url = TrackUrl("https://www.youtube.com/watch?v=UbYQErtM9Zk")
    track = Track.match_provider(track_url)
    title = track.get_title()
    assert title == (
        "Purple Disco Machine, Sophie and the Giants"
        " - Hypnotized (Official Video)"
    )


def test_track_maximum_duration():
    track_url = TrackUrl("https://www.youtube.com/watch?v=UbYQErtM9Zk")
    track = Track.match_provider(track_url)

    with patch.object(
        track,
        "get_duration",
        lambda: TestConfig.MAX_TRACK_DURATION_SECONDS - 20,
    ):
        assert track.check_valid_duration()

    with patch.object(
        track,
        "get_duration",
        lambda: TestConfig.MAX_TRACK_DURATION_SECONDS + 10,
    ):
        assert not track.check_valid_duration()
