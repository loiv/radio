from unittest import mock

from music.downloader.download import prepare_music_file
from shared.custom_types import TrackUrl
from shared.track import Track
from tests.conftest import TestConfig


@mock.patch("shared.track.Track.mark_as_downloaded")
@mock.patch("music.downloader.download.move_waiting_tracks_to_queue")
@mock.patch("music.downloader.download.check_path_exists")
@mock.patch("music.downloader.download.convert_track")
@mock.patch("music.downloader.download.download_track")
def test_do_not_download_nor_convert_track_again(
    download_mock,
    convert_mock,
    path_exists_mock,
    track_playdt_move_queue_mock,
    track_mark_downloaded_mock,
):
    track_url = TrackUrl("https://www.youtube.com/watch?v=I5xZRlQFYj0")
    path_exists_mock.return_value = True

    prepare_music_file(track_url)

    download_mock.assert_not_called()
    convert_mock.assert_not_called()
    track_playdt_move_queue_mock.assert_called()
    track_mark_downloaded_mock.assert_called()


@mock.patch("shared.os_utils.check_path_exists", return_value=True)
def test_track_files_paths_get(dir_exists_mock):
    track_url = TrackUrl("https://www.youtube.com/watch?v=BsRKiPEaRX4")
    track = Track.match_provider(track_url)
    downloaded_file_path, converted_file_path = track.get_paths()

    assert str(downloaded_file_path).endswith(track.id)
    assert TestConfig.DIRECTORY_DOWNLOAD_NAME in str(downloaded_file_path)
    assert str(converted_file_path).endswith(track.id + ".wav")
    assert TestConfig.DIRECTORY_READY_TO_PLAY_NAME in str(converted_file_path)

    dir_exists_mock.assert_called()
