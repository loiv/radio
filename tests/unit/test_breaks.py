from datetime import datetime, time, timedelta
from unittest.mock import patch

from freezegun import freeze_time
from shared.config import Config
from shared.database.values import get_breaks
from shared.time_utils import (
    check_publication_time_is_over,
    get_current_break_start_time,
    get_remaining_time_to_next_break,
    get_seconds_left_during_current_break,
)
from tests.conftest import TestConfig

today_date = datetime.now(tz=TestConfig.OUR_TIMEZONE).date()

TIME_SHIFT = 6


@patch("shared.time_utils.get_time_shift", return_value=TIME_SHIFT)
def test_current_break_start_time(_):
    breaks = get_breaks()
    expected_break_start_time = list(breaks)[1]

    datetime_official_at_break_start = datetime.combine(
        today_date, expected_break_start_time, tzinfo=TestConfig.OUR_TIMEZONE
    ) - timedelta(seconds=TIME_SHIFT)

    timepoint_official_at_break_middle = (
        datetime_official_at_break_start + timedelta(minutes=5)
    )
    datetime_official_before_break = (
        datetime_official_at_break_start - timedelta(seconds=(TIME_SHIFT // 2))
    )

    with freeze_time(datetime_official_at_break_start, tick=False):
        assert get_current_break_start_time() == expected_break_start_time

    with freeze_time(timepoint_official_at_break_middle, tick=False):
        assert get_current_break_start_time() == expected_break_start_time
    with freeze_time(datetime_official_before_break, tick=False):
        assert get_current_break_start_time() is None


@patch("shared.time_utils.get_time_shift", return_value=TIME_SHIFT)
def test_get_next_break_remaining_time_in_seconds(_):
    current_official_time = datetime.combine(
        today_date, time(9, 55), tzinfo=TestConfig.OUR_TIMEZONE
    )
    next_break_official_time = current_official_time.replace(
        hour=10, minute=20
    ) - timedelta(seconds=TIME_SHIFT)
    diff = next_break_official_time - current_official_time

    with freeze_time(current_official_time, tick=False):
        remaining_time = get_remaining_time_to_next_break()

    assert remaining_time == diff.total_seconds()


@patch("shared.time_utils.get_time_shift", return_value=TIME_SHIFT)
def test_time_left_during_break_in_seconds(_):
    current_datetime = datetime.combine(
        today_date, time(9, 31), tzinfo=TestConfig.OUR_TIMEZONE
    )
    with freeze_time(current_datetime):
        time_left = get_seconds_left_during_current_break()
    assert time_left == 4 * 60


def test_get_next_day_break_remaining_time():
    datetime_before_midnight = datetime(
        2022, 3, 14, 23, 17, 8, tzinfo=Config.OUR_TIMEZONE
    )
    datetime_after_midnight = datetime(
        2022, 3, 15, 4, 39, 2, tzinfo=Config.OUR_TIMEZONE
    )
    breaks = get_breaks()
    assert list(breaks)[0] == time(8, 30)

    with freeze_time(datetime_before_midnight):
        assert get_remaining_time_to_next_break() == 33172
    with freeze_time(datetime_after_midnight):
        assert get_remaining_time_to_next_break() == 13858


@patch("shared.time_utils.get_time_shift", return_value=TIME_SHIFT)
def test_publication_time_is_over(_):
    break_start_time = datetime.combine(
        today_date, time(9, 25), tzinfo=TestConfig.OUR_TIMEZONE
    )

    datetime_after_break_end = (
        break_start_time
        + timedelta(seconds=TIME_SHIFT)
        + timedelta(minutes=19)
    )

    with freeze_time(break_start_time):
        assert not check_publication_time_is_over(break_start_time)
    with freeze_time(datetime_after_break_end):
        assert check_publication_time_is_over(break_start_time)
