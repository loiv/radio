from unittest import mock

from radio.auth.forms import GoogleSignInTokenSubmitForm
from radio.request_utils import MyMultiDict
from tests.conftest import TestConfig
from tests.jwt_utils import (
    generate_jwt_kid,
    generate_rsa_priv_key,
    generate_x509_cert,
    get_self_signed_cert_dict,
    get_valid_token_dict,
    sign_jwt_token,
)

jwt_kid = generate_jwt_kid()
private_key = generate_rsa_priv_key()
x509_cert = generate_x509_cert(private_key)
gsi_cert_list = get_self_signed_cert_dict(jwt_kid, x509_cert)


@mock.patch(
    "radio.auth.forms._get_audience_list",
    return_value=TestConfig.GSI_CLIENTS_IDS[0],
)
@mock.patch("radio.auth.forms.get_gsi_cert_list")
def check_token_is_valid(
    unsigned_jwt_token: dict,
    valid_or_invalid: bool,
    gsi_cert_mock,
    _,
    invalid_reason=None,
):
    gsi_cert_mock.return_value = gsi_cert_list

    signed_jwt_token = sign_jwt_token(jwt_kid, unsigned_jwt_token, private_key)
    form_data = MyMultiDict({"credential": signed_jwt_token})
    form = GoogleSignInTokenSubmitForm(form_data)
    assert form.validate() is valid_or_invalid
    if invalid_reason is not None:
        form_error = form.errors["credential"]
        assert invalid_reason in str(form_error)


@mock.patch(
    "radio.auth.forms._get_audience_list",
    return_value=TestConfig.GSI_CLIENTS_IDS[0],
)
@mock.patch(
    "radio.auth.forms._get_gsi_accounts_domain",
    return_value=TestConfig.GSI_ACCOUNTS_DOMAIN,
)
def test_valid_gsi_jwt_token_validation(one, two):
    token_dict = get_valid_token_dict()
    check_token_is_valid(token_dict, True)


@mock.patch(
    "radio.auth.forms._get_audience_list",
    return_value=TestConfig.GSI_CLIENTS_IDS[0],
)
def test_invalid_issuer_gsi_jwt_token_validation(_):
    token_dict = get_valid_token_dict()
    token_dict["iss"] = "someinvaliddata_asdfkbnjkasdfbn"
    check_token_is_valid(token_dict, False)


@mock.patch(
    "radio.auth.forms._get_audience_list",
    return_value=TestConfig.GSI_CLIENTS_IDS[0],
)
def test_invalid_audience_gsi_jwt_token_validation(_):
    token_dict = get_valid_token_dict()
    token_dict["aud"] = "someinvaliddata_asdfkbnjkasdfbn"
    check_token_is_valid(token_dict, False, invalid_reason="audience")


@mock.patch(
    "radio.auth.forms._get_audience_list",
    return_value=TestConfig.GSI_CLIENTS_IDS[0],
)
@mock.patch(
    "radio.auth.forms._get_gsi_accounts_domain",
    return_value="some.example.domain.com",
)
def test_invalid_domain_gsi_jwt_token_validation(one, two):
    token_dict = get_valid_token_dict()
    token_dict["hd"] = "some.othersdafjkndskf.domain.com"
    check_token_is_valid(
        token_dict, False, invalid_reason="Niepoprawna domena konta"
    )


@mock.patch(
    "radio.auth.forms._get_audience_list",
    return_value=TestConfig.GSI_CLIENTS_IDS[0],
)
def test_expired_gsi_jwt_token_validation(_):
    token_dict = get_valid_token_dict()
    token_dict["exp"] = 1329834106
    check_token_is_valid(token_dict, False, invalid_reason="czas podpis")


@mock.patch(
    "radio.auth.forms._get_audience_list",
    return_value=TestConfig.GSI_CLIENTS_IDS[0],
)
@mock.patch("radio.auth.forms.get_gsi_cert_list")
def test_invalid_signature_gsi_jwt_token_validation(gsi_cert_mock, _):
    gsi_cert_mock.return_value = gsi_cert_list
    token_dict = get_valid_token_dict()
    properly_signed_jwt_token = sign_jwt_token(
        jwt_kid, token_dict, private_key
    )
    splitted_jwt_token = properly_signed_jwt_token.split(".")
    splitted_jwt_token[2] = "sthinvalid"
    jwt_token_with_invalid_sign = ".".join(splitted_jwt_token)
    form_data = MyMultiDict({"credential": jwt_token_with_invalid_sign})
    form = GoogleSignInTokenSubmitForm(form_data)
    assert not form.validate()
    assert "Wystąpił błąd podczas dekodowania tokenu" in str(form.errors)
