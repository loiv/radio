from unittest.mock import patch

from music.downloader.download import convert_track
from shared.custom_types import TrackUrl
from shared.track import Track


@patch("music.downloader.download.ffmpeg.run")
@patch("music.downloader.download.ffmpeg.output")
@patch("music.downloader.download.ffmpeg.input")
def test_convert_track_files(
    mocked_ffmpeg_input,
    mocked_ffmpeg_output,
    mocked_ffmpeg_run,
):
    track_url = TrackUrl("https://www.youtube.com/watch?v=uij3jxHq7PM")
    track = Track.match_provider(track_url)
    downloaded_file_path, converted_file_path = track.get_paths()
    convert_track(downloaded_file_path, converted_file_path)

    mocked_ffmpeg_input.assert_called_with(downloaded_file_path)
    call = mocked_ffmpeg_output.call_args_list[0]
    call_args_kwargs_list = [arg for arg in call.args[1:]]
    for key in call.kwargs.keys():
        call_args_kwargs_list.append(key)
    for value in call.kwargs.values():
        call_args_kwargs_list.append(value)
    assert converted_file_path in call_args_kwargs_list
    mocked_ffmpeg_run.assert_called()
