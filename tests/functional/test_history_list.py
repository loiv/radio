import json
from datetime import datetime

from flask.helpers import url_for
from tests.conftest import TestConfig


def test_get_history_list(test_client, database):
    publish_date = (
        datetime.now(tz=TestConfig.OUR_TIMEZONE).date().strftime("%Y-%m-%d")
    )
    with test_client.application.test_request_context():
        history_list_url = url_for("api.history_list")
    url = f"{history_list_url}?play-date={publish_date}"
    response = test_client.get(url)
    jsoned_response = json.loads(response.data.decode())
    assert response.status_code == 200
    assert jsoned_response
