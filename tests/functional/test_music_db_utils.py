import asyncio
from datetime import datetime, timedelta
from unittest import mock

import pytest
from freezegun import freeze_time
from music.downloader.download import download_manager, find_track_to_download
from music.player import (
    check_if_manually_stopped,
    mark_track_as_played_when_finishes,
)
from shared.config import Config
from shared.custom_types import TrackPlaying, TrackUrl
from shared.database.query_utils import update_player_status
from shared.track import Track
from tests.conftest import PointlessWaiting, TestConfig

morning_datetime = datetime.now(tz=Config.OUR_TIMEZONE).replace(
    hour=7, minute=10
)


def test_finding_track_to_download(empty_database):
    accepted_track_to_download = TestConfig.accepted_tracks[0]
    assert accepted_track_to_download["downloaded"] is False
    empty_database.library.insert_one(accepted_track_to_download)

    assert find_track_to_download() == accepted_track_to_download


def test_update_player_status_in_database(empty_database):
    update_player_status(manually_stopped=True)

    initial_status = empty_database.status.find_one({"type": "player"})

    assert check_if_manually_stopped() is True
    assert initial_status["manuallyStopped"] is True

    update_player_status(manually_stopped=False)

    assert check_if_manually_stopped() is False
    current_status = empty_database.status.find_one({"type": "player"})
    assert current_status["manuallyStopped"] is False


def test_marking_as_downloaded(empty_database):
    sample_track = TestConfig.accepted_tracks[0]
    track_url = sample_track["trackUrl"]
    track = Track.match_provider(track_url)
    empty_database.library.insert_one(sample_track)
    query = {"trackUrl": track_url}

    assert empty_database.library.find_one(query)["downloaded"] is False

    track.mark_as_downloaded()

    assert empty_database.library.find_one(query)["downloaded"] is True


async def async_download_manager():
    download_manager()


@freeze_time(morning_datetime)
@mock.patch("music.downloader.download.sleep")
@mock.patch("music.downloader.download.convert_track")
@mock.patch("music.downloader.download.download_track")
@mock.patch("music.downloader.download.find_track_to_download")
@mock.patch("shared.os_utils.create_path_if_not_exists")
@mock.patch("music.downloader.download.check_path_exists", return_value=False)
def test_marking_track_as_downloaded_after_downloading_and_converting(
    mocked_path_existence,
    mocked_creating_path,
    mocked_find_track_to_download,
    mocked_download,
    mocked_convert,
    mocked_sleep,
    empty_database,
):
    track = TestConfig.accepted_tracks[0]
    assert track["downloaded"] is False
    empty_database.library.insert_one(track)
    empty_database.waiting.insert_one(
        {
            "datetime": morning_datetime.replace(hour=8, minute=30),
            "trackUrl": track["trackUrl"],
            "weight": 1,
        }
    )

    mocked_sleep.side_effect = PointlessWaiting()
    mocked_find_track_to_download.side_effect = [track, None]

    with pytest.raises(PointlessWaiting):
        asyncio.run(async_download_manager())

    mocked_download.assert_called()
    mocked_convert.assert_called()
    assert (
        empty_database.library.find_one({"trackUrl": track["trackUrl"]})[
            "downloaded"
        ]
        is True
    )
    assert empty_database.waiting.count_documents({}) == 0
    assert empty_database.queue.count_documents({}) == 1


def test_marking_as_played_exact_datetime(empty_database):
    async def f(_):
        return True

    track_url = TrackUrl("https://www.youtube.com/watch?v=RlaBTqv8tQs")
    track = Track.match_provider(track_url)
    playing_time = datetime(2022, 3, 17, 8, 30, tzinfo=TestConfig.OUR_TIMEZONE)
    played_track = TrackPlaying(track_url, playing_time)

    track.add_to_queue_at(playing_time - timedelta(days=2))
    track.add_to_queue_at(playing_time - timedelta(days=1))
    track.add_to_queue_at(playing_time)
    track.add_to_queue_at(playing_time + timedelta(days=2))

    assert empty_database.queue.count_documents({"trackUrl": track.url}) == 4

    with mock.patch("asyncio.sleep", f):
        asyncio.run(mark_track_as_played_when_finishes(30, played_track))

    assert empty_database.queue.count_documents({"trackUrl": track.url}) == 3

    assert not empty_database.queue.find_one(
        {"trackUrl": track.url, "datetime": playing_time}
    )
    assert empty_database.history.find_one(
        {"trackUrl": track.url, "datetime": playing_time}
    )

    assert empty_database.queue.find_one(
        {"trackUrl": track.url, "datetime": playing_time - timedelta(days=2)}
    )
    assert empty_database.queue.find_one(
        {"trackUrl": track.url, "datetime": playing_time - timedelta(days=1)}
    )
    assert empty_database.queue.find_one(
        {"trackUrl": track.url, "datetime": playing_time + timedelta(days=2)}
    )
