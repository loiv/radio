from datetime import datetime, time, timedelta

from freezegun import freeze_time
from notifier.notifier import get_player_status
from shared.custom_types import TrackUrl
from shared.database.query_utils import update_player_status
from shared.database.values import get_time_shift
from tests.conftest import TestConfig


def test_get_player_status_data_when_manually_stopped(empty_database):

    update_player_status(manually_stopped=True)

    endpoint_response = get_player_status()
    assert endpoint_response["stopped"] is True


@TestConfig.my_vcr.use_cassette()
def test_get_currently_playing_track_url(empty_database):
    track_url = TrackUrl("https://www.youtube.com/watch?v=QAkulbCbWTo")
    update_player_status(track_url=track_url)
    update_player_status(manually_stopped=False)

    timepoint_during_break = datetime.combine(
        datetime.now().date(), time(9, 25), tzinfo=TestConfig.OUR_TIMEZONE
    ) + timedelta(seconds=get_time_shift() + 1)

    with freeze_time(timepoint_during_break):
        endpoint_response = get_player_status()

    assert TrackUrl(endpoint_response["trackUrl"]) == track_url
