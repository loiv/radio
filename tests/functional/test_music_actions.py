from unittest import mock

from flask import url_for
from requests.exceptions import Timeout
from tests.conftest import TestConfig
from tests.cookie_utils import authorize_by_cookie


@TestConfig.my_vcr.use_cassette()
def test_music_make_start_action(test_client, empty_database):
    with test_client.application.app_context():
        endpoint_url = url_for("music.music_start")

    authorize_by_cookie(test_client)

    response = test_client.post(endpoint_url)
    assert response.status_code == 200
    jsoned_resp = response.get_json()
    assert jsoned_resp["success"] is True


def test_music_make_action_when_timeout(test_client, empty_database):
    with test_client.application.app_context():
        endpoint_url = url_for("music.music_stop")

    authorize_by_cookie(test_client)

    with mock.patch("requests.post", side_effect=Timeout):
        response = test_client.post(endpoint_url)
        assert response.status_code == 504
        jsoned_resp = response.get_json()
        assert jsoned_resp["success"] is False
