import json
from datetime import datetime, timedelta
from unittest import mock
from zoneinfo import ZoneInfo

from flask import url_for
from freezegun import freeze_time
from music.downloader.download import prepare_music_file
from shared.config import Config, TrackLibraryStatuses
from shared.database.query_utils import (
    get_queue_weight_at,
    get_track_to_play,
    move_waiting_tracks_to_queue,
)
from shared.database.values import get_time_shift
from shared.track import Track
from tests.conftest import TestConfig
from tests.cookie_utils import authorize_by_cookie

break_datetime = TestConfig.queue_tracks_datetime

morning_datetime = datetime.now(tz=Config.OUR_TIMEZONE).replace(
    hour=7, minute=10, second=0, microsecond=0
)


def test_get_track_queue_weight(empty_database):
    for track in TestConfig.queue_tracks:
        empty_database.queue.insert_one(track)

    current_track_queue_weight = get_queue_weight_at(break_datetime)
    # weight is indexing from 1
    assert current_track_queue_weight == len(TestConfig.queue_tracks)


@freeze_time(morning_datetime)
def test_set_weight_when_no_queued_at_same_time_as_waiting(empty_database):
    track = TestConfig.pending_approval_tracks[0]
    track_url = track["trackUrl"]
    dt = morning_datetime.replace(hour=8, minute=30)
    document = {"trackUrl": track_url, "datetime": dt, "weight": 2}
    empty_database.waiting.insert_one(document)

    move_waiting_tracks_to_queue(track_url)

    found_track = empty_database.queue.find_one({"trackUrl": track_url})
    assert found_track
    assert found_track["weight"] == 1


def test_get_track_to_play(empty_database):
    track = TestConfig.queue_tracks[0]
    empty_database.queue.insert_one(track)
    with freeze_time(
        track["datetime"] + timedelta(seconds=get_time_shift() + 1)
    ):
        track_to_play = get_track_to_play()
    assert track_to_play
    assert track_to_play.url == track["trackUrl"]


def test_empty_db_get_track_queue_weight(empty_database):
    current_track_queue_weight = get_queue_weight_at(break_datetime)
    assert current_track_queue_weight == 0


def test_get_queue_tracks_list(test_client, database):
    publish_date = datetime.now(tz=TestConfig.OUR_TIMEZONE).date().isoformat()

    with test_client.application.test_request_context():
        queue_list_url = url_for("api.queue_list")
    url = f"{queue_list_url}?play-date={publish_date}"

    response = test_client.get(url)

    jsoned_response = json.loads(response.data.decode())
    keys = ("title", "duration", "datetime", "trackUrl")
    assert response.status_code == 200
    assert jsoned_response
    assert all(key in jsoned_response[0] for key in keys)


def test_delete_from_tracks_queue(test_client, database):
    authorize_by_cookie(test_client)
    track_to_delete = database.queue.find_one({})
    queued_datetime = track_to_delete["datetime"].replace(
        tzinfo=ZoneInfo("UTC")
    )
    with test_client.application.test_request_context():
        queue_delete_track_url = url_for("api.queue_delete_track")
    dumped_data = json.dumps(
        {
            "queued-datetime": queued_datetime.isoformat(),
            "track-url": track_to_delete["trackUrl"],
        }
    )
    response = test_client.delete(
        queue_delete_track_url,
        data=dumped_data,
        content_type="application/json",
    )
    assert response.status_code == 204
    assert not list(database.queue.find({"_id": track_to_delete["_id"]}))


@freeze_time(morning_datetime)
@mock.patch("music.downloader.download.check_path_exists", return_value=True)
def test_move_waiting_tracks_to_queue_when_file_prepared_respect_past_weight(
    existence_mock,
    empty_database,
):
    playing_datetime = datetime.now(tz=TestConfig.OUR_TIMEZONE).replace(
        hour=8,
        minute=30,
        second=0,
        microsecond=0,
    )
    pending_approval_track = TestConfig.pending_approval_tracks[0]
    pending_approval_track_url = pending_approval_track["trackUrl"]
    empty_database.library.insert_one(pending_approval_track)
    empty_database.queue.insert_one(
        {
            "datetime": playing_datetime,
            "trackUrl": TestConfig.accepted_tracks[1]["trackUrl"],
            "weight": 2,
        }
    )

    track = Track.match_provider(pending_approval_track_url)
    track.add_as_waiting_with_datetime_to_play(playing_datetime)

    track.update_status(TrackLibraryStatuses.ACCEPTED)

    prepare_music_file(track.url)
    existence_mock.assert_called()

    track_query = {"trackUrl": pending_approval_track_url}
    assert not list(empty_database.waiting.find(track_query))
    assert list(empty_database.queue.find(track_query))
    newly_moved_track = empty_database.queue.find_one(
        {"trackUrl": pending_approval_track_url}
    )
    assert newly_moved_track["weight"] == 2 + 1


@freeze_time(morning_datetime)
def test_moving_waiting_track_to_queue_not_overwrite(empty_database):
    pending_approve_track_data = TestConfig.pending_approval_tracks[0]
    empty_database.library.insert_one(pending_approve_track_data)
    accepted_track_data = TestConfig.accepted_tracks[0]
    empty_database.library.insert_one(accepted_track_data)

    playing_datetime = datetime.now().replace(
        hour=8,
        minute=30,
        second=0,
        microsecond=0,
        tzinfo=TestConfig.OUR_TIMEZONE,
    )
    pending_approve_track = Track.match_provider(
        pending_approve_track_data["trackUrl"]
    )
    pending_approve_track.add_as_waiting_with_datetime_to_play(
        playing_datetime
    )
    accepted_track = Track.match_provider(accepted_track_data["trackUrl"])
    accepted_track.add_to_queue_at(playing_datetime)

    assert empty_database.waiting.find_one(
        {"trackUrl": pending_approve_track.url}
    )
    assert empty_database.queue.find_one({"trackUrl": accepted_track.url})

    pending_approve_track.update_status(TrackLibraryStatuses.ACCEPTED)

    pending_approve_track.mark_as_downloaded()
    move_waiting_tracks_to_queue(pending_approve_track.url)

    assert not list(empty_database.waiting.find())
    assert len(list(empty_database.queue.find())) == 2
