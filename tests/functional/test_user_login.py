import json
from datetime import datetime
from email.utils import format_datetime
from unittest import mock

from flask import url_for
from flask_login import login_user
from radio.auth.forms import get_gsi_cert_list
from radio.auth.models import User
from tests.conftest import TestConfig
from tests.cookie_utils import delete_session_cookie
from tests.jwt_utils import (
    generate_jwt_kid,
    generate_rsa_priv_key,
    generate_x509_cert,
    get_self_signed_cert_dict,
    get_valid_token_dict,
    sign_jwt_token,
)


@mock.patch("radio.auth.routes.login_user", wraps=login_user)
@mock.patch("radio.auth.routes.User.load_user_data_from_database")
@mock.patch("radio.auth.forms.get_gsi_cert_list")
def test_user_valid_login(
    gsi_cert_mock, user_load_mock, user_login_mock, test_client
):
    jwt_kid = generate_jwt_kid()
    private_key = generate_rsa_priv_key()
    x509_cert = generate_x509_cert(private_key)
    gsi_cert_list = get_self_signed_cert_dict(jwt_kid, x509_cert)
    gsi_cert_mock.return_value = gsi_cert_list

    jwt_sub = TestConfig.user.identificator
    user = User(
        jwt_sub,
        "Jan Nowak",
        "j.nowak@g.loiv.torun.pl",
    )
    user_load_mock.return_value = user

    jwt_token_dict = get_valid_token_dict(sub=jwt_sub)
    signed_jwt_token = sign_jwt_token(jwt_kid, jwt_token_dict, private_key)
    delete_session_cookie(test_client)
    with test_client.application.test_request_context():
        endpoint_url = url_for("auth.check_google_sign_in_token")
    response = test_client.post(
        endpoint_url,
        data=json.dumps(
            {
                "credential": signed_jwt_token,
            }
        ),
        content_type="application/json",
    )
    assert response.status_code == 200
    user_login_mock.assert_called()

    cookie_header = response.headers.get("Set-Cookie")
    assert "Path=/" in cookie_header
    assert "SameSite=Strict" in cookie_header
    assert "HttpOnly" in cookie_header
    assert "Secure" in cookie_header


@mock.patch("radio.auth.routes.login_user")
@mock.patch("radio.auth.forms.get_gsi_cert_list")
def test_user_login_with_invalidly_signed_token(
    gsi_cert_mock, user_login_mock, test_client
):
    jwt_kid = generate_jwt_kid()
    private_key_used_to_sign = generate_rsa_priv_key()

    server_private_key = generate_rsa_priv_key()
    server_x509_cert = generate_x509_cert(server_private_key)
    server_gsi_cert_list = get_self_signed_cert_dict(jwt_kid, server_x509_cert)

    gsi_cert_mock.return_value = server_gsi_cert_list

    jwt_sub = TestConfig.user.identificator
    jwt_token_dict = get_valid_token_dict(sub=jwt_sub)
    signed_jwt_token = sign_jwt_token(
        jwt_kid, jwt_token_dict, private_key_used_to_sign
    )

    delete_session_cookie(test_client)

    with test_client.application.test_request_context():
        endpoint_url = url_for("auth.check_google_sign_in_token")
    response = test_client.post(
        endpoint_url,
        data=json.dumps(
            {
                "credential": signed_jwt_token,
            }
        ),
        content_type="application/json",
    )
    assert response.status_code == 422
    user_login_mock.assert_not_called()


def test_login_required(test_client):
    with test_client.application.app_context():
        # url, method, data
        endpoints = [
            (
                url_for("api.queue_delete_track"),
                "DELETE",
                {
                    "queued-datetime": format_datetime(datetime.now()),
                    "track-url": "https://www.youtube.com/watch?v=S7sN-cFhwuk",
                },
            ),
            (
                url_for("api.track_reject"),
                "PATCH",
                {"track-url": "https://www.youtube.com/watch?v=S7sN-cFhwuk"},
            ),
            (
                url_for("api.track_accept"),
                "PATCH",
                {"track-url": "https://www.youtube.com/watch?v=S7sN-cFhwuk"},
            ),
            (url_for("auth.check_admin_authenticated"), "GET", None),
            (url_for("music.music_stop"), "POST", None),
            (url_for("music.music_start"), "POST", None),
        ]
        for endpoint in endpoints:
            url, method, data = endpoint
            lower_method = method.lower()
            response = getattr(test_client, lower_method)(
                url,
                data=json.dumps(data) if data is not None else None,
                content_type="application/json",
            )
            assert response.status_code == 401
            response_data = response.get_json()
            assert response_data["success"] is False
            assert response_data["message"] == "Wymagane uwierzytelnienie"


@mock.patch("radio.auth.routes.login_user")
@mock.patch("radio.auth.forms.get_gsi_cert_list")
def test_login_when_user_not_in_database(
    gsi_cert_mock, login_user_mock, test_client, empty_database
):

    jwt_kid = generate_jwt_kid()
    private_key = generate_rsa_priv_key()
    x509_cert = generate_x509_cert(private_key)
    gsi_cert_list = get_self_signed_cert_dict(jwt_kid, x509_cert)
    gsi_cert_mock.return_value = gsi_cert_list
    jwt_token_dict = get_valid_token_dict(sub="106125607189207821229")
    signed_jwt_token = sign_jwt_token(jwt_kid, jwt_token_dict, private_key)

    with test_client.application.test_request_context():
        endpoint_url = url_for("auth.check_google_sign_in_token")

    delete_session_cookie(test_client)
    response = test_client.post(
        endpoint_url,
        data=json.dumps(
            {
                "credential": signed_jwt_token,
            }
        ),
        content_type="application/json",
    )
    assert response.status_code == 401
    assert response.headers.get("Set-Cookie") is None
    login_user_mock.assert_not_called()


def test_get_sign_in_with_google_certs(empty_database):
    with TestConfig.my_vcr.use_cassette("test_get_sign_in_with_google_certs"):
        fetched_certs = get_gsi_cert_list()

    # this time no internet request is done
    certs_from_db = get_gsi_cert_list()
    assert list(fetched_certs.keys()) == list(certs_from_db.keys())
    assert list(empty_database.certs.find())[0]["expiresAt"]
