import json

from flask import url_for


def test_get_breaks_list(test_client):
    with test_client.application.test_request_context():
        breaks_list_url = url_for("api.breaks_list")
    response = test_client.get(breaks_list_url)
    jsoned_response = json.loads(response.data.decode())
    assert response.status_code == 200
    assert jsoned_response
