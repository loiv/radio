import json

from flask import url_for
from tests.conftest import TestConfig


def test_queue_break_tracks_duration_per_break(test_client, database):
    play_datetime = TestConfig.queue_tracks_datetime
    with test_client.application.test_request_context():
        queue_tracks_duration_url = url_for("api.queue_left_time_on_breaks")
    play_date_iso = play_datetime.date().isoformat()
    url = f"{queue_tracks_duration_url}?play-date={play_date_iso}"
    response = test_client.get(url)
    jsoned_response = json.loads(response.data.decode())
    assert response.status_code == 200
    assert jsoned_response
