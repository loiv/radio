import json
import random
import string
from collections import namedtuple
from datetime import datetime, time, timedelta
from unittest import mock

from flask import url_for
from freezegun import freeze_time
from radio.request_utils import ErrorResponses, WaitResponses
from shared.config import Config, TrackLibraryStatuses
from shared.custom_types import TrackUrl
from shared.messages import TrackErrorMessages
from shared.time_utils import get_break_duration
from tests.conftest import TestConfig

default_track_url = TrackUrl("https://www.youtube.com/watch?v=UbYQErtM9Zk")

example_date = datetime(2021, 12, 28).date()
example_datetime = datetime.combine(
    example_date, time(9, 25), tzinfo=Config.OUR_TIMEZONE
)
morning_datetime = datetime.combine(
    example_date, time(7, 10), tzinfo=Config.OUR_TIMEZONE
)
timestamp_when_break_is_over = datetime.combine(
    example_date, time(10, 30), tzinfo=Config.OUR_TIMEZONE
)
timestamp_during_break = datetime.combine(
    example_date, time(10, 24), tzinfo=Config.OUR_TIMEZONE
)
TrackAddResponse = namedtuple("TrackAddResponse", ["data", "status_code"])


def prepare_track_adding_data(
    track_url: TrackUrl | str,
    play_datetime: datetime,
):
    return {
        "track-url": track_url,
        "play-datetime": play_datetime.isoformat(),
        "frc-captcha-solution": "somethinginvalidbutmocked",
    }


def send_adding_track_request(test_client, data: str):
    with test_client.application.test_request_context():
        endpoint_url = url_for("api.track_add")
    response = test_client.post(
        endpoint_url,
        data=data,
        content_type="application/json",
    )
    return response


def request_add_track(
    test_client,
    track_url: TrackUrl | str,
    play_datetime: datetime = example_datetime,
):
    with mock.patch(
        "radio.api.forms.request_captcha_validation"
    ) as mocked_captcha:
        mocked_captcha.return_value = {"success": "true"}

        prepared_data = prepare_track_adding_data(track_url, play_datetime)
        request_data = json.dumps(prepared_data)
        response = send_adding_track_request(test_client, request_data)

        decoded_response = response.data.decode()
        jsoned_response = json.loads(decoded_response)
        return TrackAddResponse(jsoned_response, response.status_code)


@freeze_time(morning_datetime)
@TestConfig.my_vcr.use_cassette()
def test_add_valid_track(test_client, database):
    valid_track_url = "https://www.youtube.com/watch?v=UbYQErtM9Zk"
    response = request_add_track(test_client, valid_track_url)
    assert response.data["success"] is True
    assert response.status_code == 202


@freeze_time(morning_datetime)
def test_add_rejected_track(test_client, database):
    rejected_track_url = TestConfig.rejected_tracks[0]["trackUrl"]
    response = request_add_track(test_client, track_url=rejected_track_url)
    assert response.data["success"] is False
    assert (
        response.data["message"] == ErrorResponses.TRACK_ALREADY_REJECTED.value
    )
    assert response.status_code == 200


@freeze_time(morning_datetime)
def test_add_pending_approval_track_not_immediately_in_queue(
    test_client,
    empty_database,
):
    pending_approval_track = TestConfig.pending_approval_tracks[0]
    pending_approval_track_url = pending_approval_track["trackUrl"]
    empty_database.library.insert_one(pending_approval_track)

    assert not list(empty_database.waiting.find())
    assert not list(empty_database.queue.find())

    response = request_add_track(
        test_client, track_url=pending_approval_track_url
    )
    assert response.status_code == 202
    assert response.data["success"] is True
    assert (
        response.data["message"]
        == WaitResponses.TRACK_ALREADY_PENDING_APPROVAL.value
    )
    assert empty_database.waiting.find_one(
        {"trackUrl": pending_approval_track_url}
    )
    assert not list(empty_database.queue.find())


@freeze_time(morning_datetime)
def test_add_accepted_not_downloaded_track(test_client, database):
    accepted_track_url = TestConfig.accepted_tracks[0]["trackUrl"]
    response = request_add_track(test_client, track_url=accepted_track_url)
    search = {"trackUrl": accepted_track_url}
    assert database.queue.count_documents(search) == 0
    assert response.data["success"] is True
    assert (
        response.data["message"]
        == WaitResponses.TRACK_ALREADY_DOWNLOADING.value
    )
    assert response.status_code == 202


@freeze_time(morning_datetime)
def test_add_accepted_track(test_client, database):
    accepted_track_url = TestConfig.accepted_tracks[1]["trackUrl"]
    response = request_add_track(test_client, track_url=accepted_track_url)
    search = {"trackUrl": accepted_track_url}
    assert database.queue.count_documents(search) == 1
    assert response.data["success"] is True
    assert response.status_code == 201


@freeze_time(timestamp_when_break_is_over)
def test_add_track_time_is_over(test_client, database):
    response = request_add_track(test_client, default_track_url)
    assert response.data["success"] is False
    assert response.data["message"] == ErrorResponses.PUBLISH_TIME_PASSED.value
    assert response.status_code == 200


@freeze_time(morning_datetime)
def test_add_track_for_weekend_day(test_client, database):
    weekend_datetime_for_track = datetime(
        2021, 12, 18, 10, 20, tzinfo=TestConfig.OUR_TIMEZONE
    )
    assert weekend_datetime_for_track.isoweekday() > 5
    with freeze_time(weekend_datetime_for_track - timedelta(minutes=12)):
        response = request_add_track(
            test_client,
            default_track_url,
            play_datetime=weekend_datetime_for_track,
        )
    assert response.data["success"] is False
    assert (
        response.data["message"]
        == ErrorResponses.DATETIME_DURING_WEEKEND.value
    )
    assert response.status_code == 200


@freeze_time(timestamp_during_break)
def test_add_track_during_break(test_client, database):
    track_url = TestConfig.accepted_tracks[1]["trackUrl"]
    response = request_add_track(
        test_client,
        track_url,
        play_datetime=example_datetime.replace(hour=10, minute=20),
    )
    assert response.data["success"] is True
    assert response.status_code == 201


@freeze_time(morning_datetime)
def test_add_track_queued_today(test_client, empty_database):
    track_url = "https://www.youtube.com/watch?v=YYhZGGMTlVU"
    empty_database.library.insert_one(
        {
            "trackUrl": track_url,
            "duration": 560,
            "title": "Freed from Desire",
            "status": TrackLibraryStatuses.ACCEPTED.value,
            "downloaded": True,
        }
    )
    empty_database.queue.insert_one(
        {
            "datetime": example_datetime,
            "trackUrl": track_url,
            "weight": 1,
        }
    )
    response = request_add_track(test_client, track_url)
    assert response.data["success"] is False
    assert (
        response.data["message"]
        == ErrorResponses.TRACK_QUEUED_FOR_THAT_DAY.value
    )
    assert response.status_code == 200


@freeze_time(morning_datetime)
def test_add_track_duration_exceeded(test_client, empty_database):
    track_url = "https://www.youtube.com/watch?v=c_iRx2Un07k"
    empty_database.library.insert_one(
        {
            "trackUrl": track_url,
            "duration": 3689,
            "title": "Kygo - Piano Jam For Studying and Sleeping [1 HOUR]",
            "status": TrackLibraryStatuses.ACCEPTED.value,
            "downloaded": False,
        },
    )
    response = request_add_track(test_client, track_url)
    assert response.data["success"] is False
    assert response.data["message"] == ErrorResponses.TRACK_TOO_LONG.value
    assert response.status_code == 200


@freeze_time(morning_datetime)
def test_add_track_when_sum_duration_exceeded_should_interrupted(
    test_client, empty_database
):
    already_queued_track_url = "https://www.youtube.com/watch?v=7emu8i2mF6I"
    track_to_add_url = "https://www.youtube.com/watch?v=wJpze4Dt7gc"
    some_other_accepted_track = TestConfig.accepted_tracks[1]
    empty_database.library.insert_one(
        {
            "trackUrl": track_to_add_url,
            "duration": 362,
            "title": "Marcin Masecki - Nokturn des dur Op27 nr 2",
            "status": TrackLibraryStatuses.ACCEPTED.value,
            "downloaded": True,
        },
    )
    empty_database.library.insert_one(some_other_accepted_track)
    empty_database.library.insert_one(
        {
            "trackUrl": already_queued_track_url,
            "duration": 382,
            "title": "Herr Mannelig",
            "status": TrackLibraryStatuses.ACCEPTED.value,
            "downloaded": True,
        },
    )
    empty_database.queue.insert_one(
        {
            "datetime": example_datetime,
            "trackUrl": already_queued_track_url,
            "weight": 1,
        }
    )
    response = request_add_track(test_client, track_to_add_url)
    # although the second track is longer than break left time,
    # it should be added and playing will be interrupted
    assert response.data["success"] is True
    assert response.status_code == 201

    empty_database.queue.insert_one(
        {
            "datetime": example_datetime,
            "trackUrl": track_to_add_url,
            "weight": 1,
        }
    )
    # unfortunately, needed to add it to a database manually
    second_try_response = request_add_track(
        test_client, some_other_accepted_track["trackUrl"]
    )
    assert second_try_response.data["success"] is False
    assert (
        second_try_response.data["message"]
        == ErrorResponses.BREAK_TIME_EXCEED.value
    )
    assert second_try_response.status_code == 200


@freeze_time(morning_datetime)
def test_add_track_max_tracks_count_queued_exceeded(
    test_client, empty_database
):
    track_url = "https://www.youtube.com/watch?v=ETMk4m-_7A0"
    yt_track_url_prefix = "https://www.youtube.com/watch?v="
    long_break_datetime = datetime.combine(
        datetime.now().date(), time(11, 15), tzinfo=TestConfig.OUR_TIMEZONE
    )
    empty_database.library.insert_one(
        {
            "trackUrl": track_url,
            "duration": 99,
            "title": "Multifandom | Dance Monkey",
            "status": TrackLibraryStatuses.ACCEPTED.value,
            "downloaded": True,
        },
    )
    for track_queue_weight in range(0, TestConfig.MAX_TRACKS_QUEUED_ONE_BREAK):
        example_track_url = yt_track_url_prefix + "".join(
            random.choices(string.ascii_lowercase, k=11)
        )
        empty_database.queue.insert_one(
            {
                "datetime": long_break_datetime,
                "trackUrl": example_track_url,
                "weight": track_queue_weight,
            }
        )
    response = request_add_track(test_client, track_url, long_break_datetime)
    assert response.data["success"] is False
    assert (
        response.data["message"]
        == ErrorResponses.MAX_TRACK_QUEUED_EXCEED.value
    )
    assert response.status_code == 200


def test_add_track_when_url_invalid(test_client):
    track_url = "https://www.youtube.com/watch?v=ETMk4m-_7A0additionalchars"
    response = request_add_track(test_client, track_url)
    assert response.data["success"] is False
    assert (
        response.data["description"]["track-url"][0]
        == TrackErrorMessages.INVALID_YOUTUBE_TRACK_URL
    )
    assert response.status_code == 422


@freeze_time(morning_datetime)
@mock.patch("radio.api.forms.request_captcha_validation")
def test_add_track_when_captcha_not_sent(mocked_captcha, test_client):
    track_url = "https://www.youtube.com/watch?v=ETMk4m-_7A0"
    request_default_data = prepare_track_adding_data(
        track_url, example_datetime
    )
    request_default_data.pop("frc-captcha-solution")
    response = send_adding_track_request(
        test_client, json.dumps(request_default_data)
    )
    mocked_captcha.assert_not_called()
    assert response.status_code == 201


@freeze_time(morning_datetime)
@mock.patch("radio.api.forms.request_captcha_validation")
def test_add_track_when_captcha_invalid(mocked_captcha, test_client):
    track_url = "https://www.youtube.com/watch?v=ETMk4m-_7A0"
    mocked_captcha.return_value = {"success": False}
    request_default_data = prepare_track_adding_data(
        track_url, example_datetime
    )
    response = send_adding_track_request(
        test_client, json.dumps(request_default_data)
    )
    assert response.status_code == 422
    mocked_captcha.assert_called()


@freeze_time(morning_datetime)
def test_add_track_to_waiting_already_requested(test_client, empty_database):
    empty_database.library.insert_one(TestConfig.pending_approval_tracks[0])
    track_url = TestConfig.pending_approval_tracks[0]["trackUrl"]

    second_break_dt = morning_datetime.replace(hour=10, minute=20)
    third_break_dt = morning_datetime.replace(hour=11, minute=15)

    response_to_on_second = request_add_track(
        test_client, track_url, play_datetime=second_break_dt
    )
    assert response_to_on_second.status_code == 202

    response_to_on_third = request_add_track(
        test_client, track_url, play_datetime=third_break_dt
    )
    assert response_to_on_third.status_code == 200
    assert (
        response_to_on_third.data["message"]
        == ErrorResponses.TRACK_POSSIBLY_SCHEDULED_THAT_DAY.value
    )


@freeze_time(morning_datetime)
def test_add_track_when_playing_sum_of_queued_and_played_tracks_exceeds_limit(
    test_client, empty_database
):
    empty_database.library.insert_many(TestConfig.accepted_tracks)

    empty_database.history.insert_one(
        {
            "datetime": example_datetime,
            "trackUrl": TestConfig.accepted_tracks[0]["trackUrl"],
        }
    )
    empty_database.queue.insert_one(
        {
            "datetime": example_datetime,
            "trackUrl": TestConfig.accepted_tracks[1]["trackUrl"],
            "weight": 1,
        }
    )
    break_start_time = example_datetime.astimezone(
        tz=Config.OUR_TIMEZONE
    ).time()
    break_duration = get_break_duration(break_start_time)
    played_and_queued_tracks_duration = (
        TestConfig.accepted_tracks[0]["duration"]
        + TestConfig.accepted_tracks[1]["duration"]
    )
    assert break_duration < played_and_queued_tracks_duration

    track_url = TestConfig.accepted_tracks[2]["trackUrl"]
    response = request_add_track(
        test_client, track_url, play_datetime=example_datetime
    )
    assert response.status_code == 200
    assert response.data["message"] == ErrorResponses.BREAK_TIME_EXCEED.value


@freeze_time(morning_datetime)
def test_set_track_weight_queue_max_existent_not_items_count(
    test_client, empty_database
):
    empty_database.library.insert_many(TestConfig.accepted_tracks[:2])
    empty_database.queue.insert_one(
        {
            "datetime": example_datetime,
            "trackUrl": TestConfig.accepted_tracks[0]["trackUrl"],
            "weight": 3,
        }
    )
    track_to_add_url = TestConfig.accepted_tracks[1]["trackUrl"]
    response = request_add_track(
        test_client, track_to_add_url, play_datetime=example_datetime
    )
    assert response.status_code == 201
    assert (
        empty_database.queue.find_one({"trackUrl": track_to_add_url})["weight"]
        == 3 + 1
    )
