import json

from flask import url_for
from tests.conftest import TestConfig
from tests.cookie_utils import authorize_by_cookie


def test_listing_admin_accounts(test_client, empty_database):
    with test_client.application.app_context():
        endpoint_url = url_for("accounts.list_admin_accounts")

    authorize_by_cookie(test_client)

    response = test_client.get(endpoint_url)
    assert response.status_code == 200
    listed_accounts = response.get_json()
    account1 = listed_accounts[0]
    assert account1["identificator"] == TestConfig.user.identificator
    assert account1["email"] == TestConfig.user.email
    assert account1["name"] == TestConfig.user.name


def test_adding_admin_account(test_client, empty_database):
    with test_client.application.app_context():
        endpoint_url = url_for("accounts.add_admin_account")
    authorize_by_cookie(test_client)

    account_identificator = "104432123456781767330"
    user_data = {
        "identificator": account_identificator,
        "email": "a.kowalczyk@g.loiv.torun.pl",
        "name": "Agata Kowalczyk",
    }

    response = test_client.post(
        endpoint_url,
        data=json.dumps(user_data),
        content_type="application/json",
    )
    assert response.status_code == 204

    user_data["email"] = "a.bcdef@g.loiv.torun.pl"
    response = test_client.post(
        endpoint_url,
        data=json.dumps(user_data),
        content_type="application/json",
    )
    assert response.status_code == 409

    assert empty_database.users.find_one(
        {"identificator": account_identificator}
    )


def test_deleting_admin_account_idempotent(test_client, empty_database):
    with test_client.application.app_context():
        endpoint_url = url_for("accounts.delete_admin_account")
    authorize_by_cookie(test_client)

    account_email = "a.kowalczyk@g.loiv.torun.pl"
    user_data = {
        "identificator": "104432123456781767330",
        "email": account_email,
        "name": "Agata Kowalczyk",
    }
    empty_database.users.insert_one(user_data)

    response = test_client.delete(
        endpoint_url,
        data=json.dumps({"email": account_email}),
        content_type="application/json",
    )
    assert response.status_code == 204

    response = test_client.delete(
        endpoint_url,
        data=json.dumps({"email": account_email}),
        content_type="application/json",
    )
    assert response.status_code == 204

    assert not empty_database.users.find_one({"email": account_email})
