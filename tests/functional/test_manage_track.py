import json

from flask import url_for
from shared.config import TrackLibraryStatuses
from tests.conftest import TestConfig
from tests.cookie_utils import authorize_by_cookie, delete_session_cookie

accepted_track = TestConfig.accepted_tracks[0]
pending_approval_track = TestConfig.pending_approval_tracks[0]
rejected_track = TestConfig.rejected_tracks[0]


def send_manage_track_request(test_client, track_url, action):
    with test_client.application.test_request_context():
        endpoint_url = url_for(f"api.track_{action}")
    response = test_client.patch(
        endpoint_url,
        data=json.dumps({"track-url": track_url}),
        content_type="application/json",
    )
    return response


def test_authorized_reject_track(test_client, empty_database):
    authorize_by_cookie(test_client)

    empty_database.library.insert_one(pending_approval_track)
    track_url = pending_approval_track["trackUrl"]

    added_track_id = empty_database.library.find_one({"trackUrl": track_url})[
        "_id"
    ]
    response = send_manage_track_request(test_client, track_url, "reject")
    should_be_rejected_track = empty_database.library.find_one(
        {"_id": added_track_id}
    )
    assert response.status_code == 204
    assert (
        should_be_rejected_track["status"]
        == TrackLibraryStatuses.REJECTED.value
    )


def test_unauthorized_reject_track(test_client, empty_database):
    delete_session_cookie(test_client)

    empty_database.library.insert_one(accepted_track)
    previous_track_status = accepted_track["status"]
    track_url = accepted_track["trackUrl"]

    added_track_id = empty_database.library.find_one({"trackUrl": track_url})[
        "_id"
    ]
    response = send_manage_track_request(test_client, track_url, "reject")
    should_not_be_rejected_track = empty_database.library.find_one(
        {"_id": added_track_id}
    )
    assert response.status_code == 401
    assert previous_track_status == should_not_be_rejected_track["status"]


def test_authorized_accept_track(test_client, empty_database):
    authorize_by_cookie(test_client)

    empty_database.library.insert_one(rejected_track)
    track_url = rejected_track["trackUrl"]
    added_track_id = empty_database.library.find_one({"trackUrl": track_url})[
        "_id"
    ]

    response = send_manage_track_request(test_client, track_url, "accept")
    should_be_accepted_track = empty_database.library.find_one(
        {"_id": added_track_id}
    )
    assert response.status_code == 204
    assert (
        should_be_accepted_track["status"]
        == TrackLibraryStatuses.ACCEPTED.value
    )


def test_unauthorized_accept_track(test_client, empty_database):
    delete_session_cookie(test_client)

    empty_database.library.insert_one(rejected_track)
    previous_track_status = rejected_track["status"]
    track_url = rejected_track["trackUrl"]

    added_track_id = empty_database.library.find_one({"trackUrl": track_url})[
        "_id"
    ]
    response = send_manage_track_request(test_client, track_url, "accept")
    should_not_be_accepted_track = empty_database.library.find_one(
        {"_id": added_track_id}
    )
    assert response.status_code == 401
    assert should_not_be_accepted_track["status"] == previous_track_status
