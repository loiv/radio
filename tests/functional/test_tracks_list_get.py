import json
from typing import Optional, TypedDict
from urllib.parse import urlencode

from flask import url_for
from shared.config import TrackLibraryStatuses

QueryParams = TypedDict(
    "QueryParams",
    {
        "offset": int,
        "show-accepted": bool,
        "show-pending-approve": bool,
        "show-rejected": bool,
        "downloaded": bool,
        "search": str,
    },
    total=False,
)


def get_tracks_list(
    test_client,
    show_accepted: bool,
    show_pending_approve: bool,
    show_rejected: bool = False,
    downloaded: Optional[bool] = None,
    search: str = "",
    offset: int = 0,
):
    params: QueryParams = {
        "offset": offset,
        "show-accepted": show_accepted,
        "show-pending-approve": show_pending_approve,
        "show-rejected": show_rejected,
    }
    if downloaded is not None:
        params["downloaded"] = downloaded
    if search:
        params["search"] = search

    with test_client.application.test_request_context():
        tracks_list_url = url_for("api.tracks_list")

    query_params_with_lowered_booleans = [
        (k, str(v).lower() if isinstance(v, bool) else v)
        for k, v in params.items()
    ]
    url = f"{tracks_list_url}?{urlencode(query_params_with_lowered_booleans)}"

    response = test_client.get(url)
    decoded_response = response.data.decode()
    listed_response = json.loads(decoded_response)["tracks"]
    assert isinstance(listed_response, list)
    return listed_response


def test_pending_approval_tracks_list_get(test_client, database):
    tracks_list = get_tracks_list(
        test_client, show_accepted=False, show_pending_approve=True
    )
    for track in tracks_list:
        assert track["status"] == TrackLibraryStatuses.PENDING_APPROVAL.value
        assert "duration" in track
        assert "title" in track
        assert "trackUrl" in track


def test_searching_in_tracks_list(test_client, database):
    tracks_list = get_tracks_list(
        test_client,
        show_accepted=False,
        show_pending_approve=True,
        search="never",
    )
    assert len(tracks_list) >= 1


def test_downloaded_tracks_list_get(test_client, database):
    tracks_list = get_tracks_list(
        test_client,
        show_accepted=True,
        show_pending_approve=True,
        downloaded=True,
    )
    assert len(tracks_list) >= 1
    first_track = tracks_list[0]
    found_track = database.library.find_one(
        {"trackUrl": first_track["trackUrl"]}
    )
    assert found_track["downloaded"] is True


def test_not_downloaded_tracks_list_get(test_client, database):
    tracks_list = get_tracks_list(
        test_client,
        show_accepted=False,
        show_pending_approve=True,
        downloaded=False,
    )
    assert len(tracks_list) >= 1
    first_track = tracks_list[0]
    found_track = database.library.find_one(
        {"trackUrl": first_track["trackUrl"]}
    )
    assert found_track["downloaded"] is False


def test_tracks_list_at_third_page(test_client, database):
    tracks_list = get_tracks_list(
        test_client, show_accepted=True, show_pending_approve=True, offset=3
    )
    assert tracks_list is not None
