from radio.auth.models import User
from shared.database.connection import connect_to_database
from shared.database.setup_utils import initialize_database

client = connect_to_database()
database = client["radio"]
initialize_database(database)

database["users"].delete_many({})
new_user = User(
    "114432748757061767330",
    "Jan Nowak",
    "j.nowak@g.loiv.torun.pl",
)
new_user.save_to_database()
