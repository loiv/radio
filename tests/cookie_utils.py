# https://gist.github.com/aescalana/7e0bc39b95baa334074707f73bc64bfe
# Also see https://github.com/onyxcherry/ManageFlaskLoginCookies
from flask.sessions import SecureCookieSessionInterface
from itsdangerous import URLSafeTimedSerializer
from tests.conftest import TestConfig


class SimpleSecureCookieSessionInterface(SecureCookieSessionInterface):
    # Override method
    # Take secret_key instead of an instance of a Flask app
    def get_signing_serializer(self, secret_key):
        if not secret_key:
            return None
        signer_kwargs = dict(
            key_derivation=self.key_derivation,
            digest_method=self.digest_method,
        )
        return URLSafeTimedSerializer(
            secret_key,
            salt=self.salt,
            serializer=self.serializer,
            signer_kwargs=signer_kwargs,
        )


# Keep in mind that flask uses unicode strings for the
# dictionary keys
def encode_flask_cookie(secret_key, cookieDict):
    sscsi = SimpleSecureCookieSessionInterface()
    signingSerializer = sscsi.get_signing_serializer(secret_key)
    assert signingSerializer
    return signingSerializer.dumps(cookieDict)


def delete_session_cookie(test_client):
    test_client.delete_cookie(
        server_name=TestConfig.SERVER_NAME, key="session"
    )
    test_client.delete_cookie(
        server_name="." + TestConfig.SERVER_NAME, key="session"
    )


def authorize_by_cookie(test_client):
    delete_session_cookie(test_client)
    app_secret_key = test_client.application.config.get("SECRET_KEY")
    # see https://github.com/onyxcherry/ManageFlaskLoginCookies#background
    cookie_session_id = (
        "47216d52c11568dc674affdd43a26e1f961589481169e08ed8d7151f5b2e8031"
        "e069126a006ce0c2e8798acd8cdcfb4fc2f32ffa9b66ff2ffd83ec82c1cc5a0e"
    )
    cookie = encode_flask_cookie(
        app_secret_key,
        {
            "_fresh": True,
            "_id": cookie_session_id,
            "_user_id": TestConfig.user.identificator,
        },
    )
    test_client.set_cookie(
        server_name=TestConfig.SERVER_NAME, key="session", value=cookie
    )
