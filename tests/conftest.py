from datetime import datetime, time

import pytest
import vcr
from radio import create_app
from radio.auth.models import User
from shared.config import Config, TrackLibraryStatuses


class PointlessWaiting(Exception):
    pass


class TestConfig(Config):
    TESTING = True
    WTF_CSRF_ENABLED = False
    WTF_CSRF_METHODS = []  # type: ignore

    SECRET_KEY = "somethingsecret"

    SERVER_NAME = "localhost.local"

    GSI_CLIENTS_IDS = [
        "846183730541-78m80n3q78tukkvcccf25tarv1b2vqtg"
        + ".apps.googleusercontent.com"
    ]

    GSI_ACCOUNTS_DOMAIN = "g.loiv.torun.pl"

    my_vcr = vcr.VCR(
        record_mode="none",
        cassette_library_dir="tests/cassettes",
        match_on=["uri", "method"],
        decode_compressed_response=True,
    )
    queue_tracks_datetime = datetime.combine(
        datetime.now().date(), time(13, 10), tzinfo=Config.OUR_TIMEZONE
    )
    history_tracks_datetime = datetime.combine(
        datetime.now().date(), time(14, 5), tzinfo=Config.OUR_TIMEZONE
    )
    rejected_tracks = [
        {
            "trackUrl": "https://www.youtube.com/watch?v=egjutxti4wc",
            "duration": 171,
            "title": "Kungs - Never Going Home (Lyrics Video)",
            "status": TrackLibraryStatuses.REJECTED.value,
            "downloaded": False,
        },
        {
            "trackUrl": "https://www.youtube.com/watch?v=BsRKiPEaRX4",
            "duration": 241,
            "title": "Therapie TAXI - Candide Crush (Clip Officiel)",
            "status": TrackLibraryStatuses.REJECTED.value,
            "downloaded": False,
        },
    ]
    pending_approval_tracks = [
        {
            "trackUrl": "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
            "duration": 213,
            "title": (
                "Rick Astley - Never Gonna Give You Up"
                " (Official Music Video)"
            ),
            "status": TrackLibraryStatuses.PENDING_APPROVAL.value,
            "downloaded": False,
        },
        {
            "trackUrl": "https://www.youtube.com/watch?v=SsdkvYdSzlg",
            "duration": 226,
            "title": "Tom Grennan - Little Bit of Love (Official Video)",
            "status": TrackLibraryStatuses.PENDING_APPROVAL.value,
            "downloaded": False,
        },
    ]
    accepted_tracks = [
        {
            "trackUrl": "https://www.youtube.com/watch?v=Q4_CemiCbTg",
            "duration": 193,
            "title": "47Ter -  J'essaie (Clip Officiel)",
            "status": TrackLibraryStatuses.ACCEPTED.value,
            "downloaded": False,
        },
        {
            "trackUrl": "https://www.youtube.com/watch?v=YYhZGGMTlVU",
            "duration": 560,
            "title": "Freed from Desire",
            "status": TrackLibraryStatuses.ACCEPTED.value,
            "downloaded": True,
        },
        {
            "trackUrl": "https://www.youtube.com/watch?v=I5xZRlQFYj0",
            "duration": 166,
            "title": "Drenchill ft. Indiiana - Never Never (Official Video)",
            "status": TrackLibraryStatuses.ACCEPTED.value,
            "downloaded": False,
        },
    ]
    queue_tracks = [
        {
            "trackUrl": "https://www.youtube.com/watch?v=SsdkvYdSzlg",
            "weight": 1,
            "datetime": queue_tracks_datetime,
        },
        {
            "trackUrl": "https://www.youtube.com/watch?v=BsRKiPEaRX4",
            "weight": 2,
            "datetime": queue_tracks_datetime,
        },
        {
            "trackUrl": "https://www.youtube.com/watch?v=I5xZRlQFYj0",
            "weight": 3,
            "datetime": queue_tracks_datetime,
        },
    ]

    history_tracks = [
        {
            "trackUrl": "https://www.youtube.com/watch?v=SsdkvYdSzlg",
            "datetime": history_tracks_datetime,
        },
        {
            "trackUrl": "https://www.youtube.com/watch?v=BsRKiPEaRX4",
            "datetime": history_tracks_datetime,
        },
    ]

    user = User(
        "114432748757061767330",
        "Jan Nowak",
        "j.nowak@g.loiv.torun.pl",
    )

    SESSION_COOKIE_SECURE = True


@pytest.fixture(scope="module")
def test_client():
    my_radio_app = create_app(TestConfig)

    testing_client = my_radio_app.test_client()

    app_context = my_radio_app.app_context()
    app_context.push()

    yield testing_client

    app_context.pop()


@pytest.fixture(scope="function")
def database():
    from shared.database.connection import db, history, library, queue, waiting

    history.delete_many({})
    history.insert_many(TestConfig.history_tracks)

    waiting.delete_many({})

    queue.delete_many({})
    queue.insert_many(TestConfig.queue_tracks)

    library.delete_many({})
    library.insert_many(TestConfig.accepted_tracks)
    library.insert_many(TestConfig.pending_approval_tracks)
    library.insert_many(TestConfig.rejected_tracks)

    return db


@pytest.fixture(scope="function")
def empty_database():
    from shared.database.connection import (
        db,
        history,
        library,
        queue,
        users,
        waiting,
    )

    history.delete_many({})
    waiting.delete_many({})
    queue.delete_many({})
    library.delete_many({})
    users.delete_many({})

    TestConfig.user.save_to_database()

    return db
