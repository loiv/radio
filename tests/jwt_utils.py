from datetime import datetime, timedelta, timezone
from random import getrandbits

import jwt
from cryptography import x509
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.x509 import Certificate
from cryptography.x509.oid import NameOID
from tests.conftest import TestConfig

# https://github.com/jpadilla/pyjwt/issues/321#issuecomment-354126200
now = datetime.utcnow().replace(tzinfo=timezone.utc)
jwt_nbf = int((now - timedelta(hours=23)).timestamp())
jwt_iat = int(now.timestamp())
jwt_exp = int((now + timedelta(hours=1)).timestamp())


def generate_jwt_kid() -> str:
    jwt_kid = hex(getrandbits(4 * 40))[2:]
    return jwt_kid


def get_valid_token_dict(sub: str = "123456789012345678901") -> dict:
    valid_token_dict = {
        "iss": "https://accounts.google.com",
        "nbf": jwt_nbf,
        "aud": TestConfig.GSI_CLIENTS_IDS[0],
        "sub": sub,
        "hd": TestConfig.GSI_ACCOUNTS_DOMAIN,
        "email": f"t.wisniewski@{TestConfig.GSI_ACCOUNTS_DOMAIN}",
        "email_verified": True,
        "azp": f"846183730541-{getrandbits(4*32)}.apps.googleusercontent.com",
        "name": "Tomasz Wiśniewski",
        "picture": "https://lh3.googleusercontent.com/a-/somepicaddress",
        "given_name": "Tomasz",
        "family_name": "Wiśniewski",
        "iat": jwt_iat,
        "exp": jwt_exp,
        "jti": hex(getrandbits(4 * 40))[2:],
    }
    return valid_token_dict


def generate_rsa_priv_key() -> rsa.RSAPrivateKey:
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
    )
    return private_key


def generate_x509_cert(private_key: rsa.RSAPrivateKey) -> Certificate:
    subject = issuer = x509.Name(
        [
            x509.NameAttribute(NameOID.COUNTRY_NAME, "US"),
            x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, "California"),
            x509.NameAttribute(NameOID.LOCALITY_NAME, "San Francisco"),
            x509.NameAttribute(NameOID.ORGANIZATION_NAME, "My Company"),
            x509.NameAttribute(NameOID.COMMON_NAME, "mysite.com"),
        ]
    )
    cert = (
        x509.CertificateBuilder()
        .subject_name(subject)
        .issuer_name(issuer)
        .public_key(private_key.public_key())
        .serial_number(x509.random_serial_number())
        .not_valid_before(datetime.utcnow())
        .not_valid_after(datetime.utcnow() + timedelta(days=10))
        .sign(private_key, hashes.SHA256())
    )
    return cert


def get_self_signed_cert_dict(jwt_kid: str, cert: Certificate) -> dict:
    certs = {
        jwt_kid: cert.public_bytes(
            encoding=serialization.Encoding.PEM
        ).decode()
    }
    return certs


def sign_jwt_token(
    jwt_kid: str, jwt_token_dict: dict, private_key: rsa.RSAPrivateKey
) -> str:
    signed_jwt_token = jwt.encode(
        jwt_token_dict,
        private_key,  # type: ignore
        algorithm="RS256",
        headers={"kid": jwt_kid},
    )
    if isinstance(signed_jwt_token, bytes):
        signed_jwt_token = signed_jwt_token.decode()
    return signed_jwt_token
