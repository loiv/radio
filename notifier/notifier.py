import asyncio
import json
from asyncio import Task

import janus
from aiohttp import web
from aiohttp.web import Request, Response
from aiohttp_sse import EventSourceResponse, sse_response
from shared.config import get_logger
from shared.custom_types import (
    PlayerStatusResponse,
    PlayerStatusUpdate,
    TrackUrl,
    _PlayerStatusNotStoppedResp,
    _PlayerStatusStoppedResp,
)
from shared.database.query_utils import get_player_status_data
from shared.time_utils import get_current_break_start_time
from shared.track import Track

logger = get_logger(__name__)


def watch_changes(sync_q: janus.SyncQueue[PlayerStatusUpdate]) -> None:
    from shared.database.connection import status as db_status

    match_condition = [
        {"$match": {"operationType": "update", "ns.coll": "status"}}
    ]
    with db_status.watch(match_condition) as stream:
        for change in stream:
            updated_fields: PlayerStatusUpdate
            updated_fields = change["updateDescription"]["updatedFields"]
            add_title(updated_fields)
            sync_q.put(updated_fields)
        sync_q.join()


async def message_forwarder(
    async_q: janus.AsyncQueue[PlayerStatusUpdate],
    client_queues: list[asyncio.Queue],
) -> None:
    while True:
        new_message = await async_q.get()
        async_q.task_done()

        for queue in client_queues:
            await queue.put(new_message)


async def notify_status_change(request: Request) -> Response:
    async with sse_response(request) as response:
        app = request.app
        another_queue = asyncio.Queue()
        app["clients"].add(response)
        app["queues"].add(another_queue)

        current_status = get_player_status()
        json_response = json.dumps(current_status)

        try:
            await response.send(json_response)

            while not response.task.done():
                message = await another_queue.get()
                await response.send(json.dumps(message))
                another_queue.task_done()
        finally:
            app["queues"].remove(another_queue)
            app["clients"].remove(response)
    return response


def add_title(
    fields: _PlayerStatusNotStoppedResp | PlayerStatusUpdate,
) -> None:
    new_track_url = fields.get("trackUrl")
    if new_track_url is not None:
        title = (
            Track.match_provider(TrackUrl(new_track_url)).get_title()
            if new_track_url
            else ""
        )
        fields["title"] = title


# pyright: reportGeneralTypeIssues=false
def get_player_status() -> PlayerStatusResponse:
    player_status = get_player_status_data()
    if player_status is None:
        raise Exception(
            "Invalid player status - incomplete information in the database"
        )

    currently_during_break = get_current_break_start_time() is not None

    if player_status["manuallyStopped"]:
        response: _PlayerStatusStoppedResp = {
            **player_status,
            "stopped": True,
            "reason": "Odtwarzanie ręcznie zatrzymane",
            "duringBreak": currently_during_break,
        }
    elif player_status["errorOccured"]:
        response: _PlayerStatusStoppedResp = {
            **player_status,
            "stopped": True,
            "reason": "Wystąpił systemowy błąd podczas odtwarzania muzyki",
        }
    elif not currently_during_break:
        response: _PlayerStatusStoppedResp = {
            **player_status,
            "stopped": True,
            "reason": "Muzykę odtwarzamy tylko podczas przerw",
            "duringBreak": currently_during_break,
        }
    else:
        response: _PlayerStatusNotStoppedResp = {
            **player_status,
            "stopped": False,
            "title": "",
            "duringBreak": currently_during_break,
        }
        add_title(response)

    return response


async def status(request: Request) -> Response:
    response = get_player_status()
    return web.json_response(response, status=200)


async def check_health(request: Request) -> Response:
    return web.json_response({"success": True}, status=200)


async def on_shutdown(app) -> None:
    for client in app["clients"]:
        client: EventSourceResponse
        message = json.dumps({"server": "stopping"})
        await client.send(message)
        client.stop_streaming()
    logger.info("Successfully closed all connections to clients")
    background_task: Task = app["background_task"]
    background_task.cancel()

    queue: janus.Queue[PlayerStatusUpdate] = app["janus_queue"]

    queue.close()
    await queue.wait_closed()
    logger.info("Closed the Janus queues")


async def start_background_tasks(app) -> None:
    asyncio.get_running_loop()
    queue: janus.Queue[PlayerStatusUpdate] = janus.Queue()
    app["janus_queue"] = queue

    app["background_task"] = asyncio.create_task(
        start_forwarding_messages(app)
    )


async def start_forwarding_messages(app) -> None:
    client_messages_queues = app["queues"]
    queue = app["janus_queue"]
    loop = asyncio.get_running_loop()

    fut = loop.run_in_executor(None, watch_changes, queue.sync_q)
    task = asyncio.create_task(
        message_forwarder(queue.async_q, client_messages_queues)
    )

    await asyncio.gather(task, fut, return_exceptions=True)


async def my_web_app() -> web.Application:
    app = web.Application()
    app.add_routes(
        [
            web.get("/music/status", status),
            web.get("/music/status/notify", notify_status_change),
            web.get("/music/status/health", check_health),
        ]
    )

    app["clients"] = set()
    app["queues"] = set()
    app["janus_queue"] = None

    app.on_startup.append(start_background_tasks)
    app.on_shutdown.append(on_shutdown)

    return app
