import asyncio
import wave
from asyncio.tasks import Task
from dataclasses import dataclass
from datetime import datetime
from typing import Final, Optional

import simpleaudio
from aiohttp import web
from aiohttp.web_request import Request
from aiohttp.web_response import Response
from shared.config import Config, get_logger
from shared.custom_types import NoTrackPlaying, TrackPlaying
from shared.database.query_utils import (
    get_player_status_data,
    get_track_to_play,
    update_player_status,
)
from shared.os_utils import check_path_exists
from shared.time_utils import (
    get_current_break_start_time,
    get_remaining_time_to_next_break,
    get_seconds_left_during_current_break,
)
from shared.track import Track

routes: Final = web.RouteTableDef()

logger: Final = get_logger(__name__)

stream: Optional[simpleaudio.PlayObject] = None
play_loop_task: Optional[Task] = None

tasks_marking_as_played: set[Task] = set()

got_track_to_play: bool = False


@dataclass
class ActionResult:
    success: bool
    by_caller: bool


def check_stereo_playback() -> None:
    import simpleaudio.functionchecks as fc

    fc.LeftRightCheck.run(countdown=0)


def stop_stream() -> bool:
    global stream
    if isinstance(stream, simpleaudio.PlayObject):
        stream.stop()  # type: ignore
        stream = None
        return True
    return False


def check_if_manually_stopped() -> bool:
    status = get_player_status_data()
    if (
        status
        and isinstance(value := status.get("manuallyStopped"), bool)
        and value is False
    ):
        # it is better to not to start playing (in case of any error)
        # than play a track unwillingly
        return False
    else:
        return True


async def mark_track_as_played_when_finishes(
    seconds: int, played_track: TrackPlaying
) -> None:
    from shared.database.connection import client, queue
    from shared.track import Track

    track = Track.match_provider(played_track.url)

    await asyncio.sleep(max(1, seconds - 3))

    with client.start_session() as session:
        with session.start_transaction():
            queue.find_one_and_delete(
                {"trackUrl": played_track.url, "datetime": played_track.when}
            )
            track.add_to_playing_history_at(played_track.when)
    logger.info(
        f"Removed track {played_track} from queue as it was played "
        + "and added to history"
    )


def create_task_to_mark_track_as_played(
    playing_duration: int, track: TrackPlaying
) -> None:
    task = asyncio.create_task(
        mark_track_as_played_when_finishes(playing_duration, track)
    )
    tasks_marking_as_played.add(task)
    task.add_done_callback(tasks_marking_as_played.discard)


def stop_music_due_to_end_of_current_break():
    stopped = stop_stream()
    if stopped:
        update_player_status(track_url=NoTrackPlaying(""))
        logger.info("The break is over – stopped playing music")


def start_playing_track(track_path: str, playing_duration: int) -> bool:
    global stream

    wave_read = wave.open(track_path, "rb")
    audio_data = wave_read.readframes(
        playing_duration * wave_read.getframerate()
    )
    num_channels = wave_read.getnchannels()
    bytes_per_sample = wave_read.getsampwidth()
    sample_rate = wave_read.getframerate()

    try:
        wav_stream = simpleaudio.WaveObject(
            audio_data, num_channels, bytes_per_sample, sample_rate
        )
        stream = wav_stream.play()
    except simpleaudio._simpleaudio.SimpleaudioError as ex:
        logger.exception(ex)
        return False

    return True


async def until_music_should_play() -> None:
    remaining_time = get_remaining_time_to_next_break()
    time_to_wait = max(remaining_time, 1)
    logger.debug(f"Will wait {time_to_wait=}")
    await asyncio.sleep(time_to_wait)


async def play_track() -> None:
    global got_track_to_play

    while True:
        if stream and stream.is_playing():
            await asyncio.sleep(1)
            continue

        current_break_start_time = get_current_break_start_time()
        logger.debug(f"{current_break_start_time=}")

        if current_break_start_time is None:
            update_player_status(during_break=False)
            stop_music_due_to_end_of_current_break()

            await until_music_should_play()
            continue

        if check_if_manually_stopped():
            await asyncio.sleep(0.1)
            continue

        update_player_status(during_break=True)

        track_to_play = get_track_to_play()
        if not track_to_play:
            if got_track_to_play:
                update_player_status(track_url=NoTrackPlaying(""))
                logger.info("Did not scheduled any next track to play")
                got_track_to_play = False

            await asyncio.sleep(0.5)
            continue

        if len(tasks_marking_as_played) > 0:
            msg = (
                "It is still an undone task to mark a track as played."
                + " Got to play {track_to_play} cannot be played. Keep waiting"
            )
            logger.warning(msg)
            await asyncio.gather(*tasks_marking_as_played)
            continue

        logger.info(f"Want to play {track_to_play}")
        got_track_to_play = True

        track = Track.match_provider(track_to_play.url)

        _, track_path = track.get_paths()
        if not check_path_exists(track_path):
            logger.warning(
                f"The path {track_path} does not exist, cannot play the track"
            )
            track.unmark_as_downloaded()
            logger.info(f"Removed the status of the downloaded track {track}")
            await asyncio.sleep(5)
            continue

        break_left_time = get_seconds_left_during_current_break()
        track_duration = track.get_duration()
        playing_duration = min(break_left_time, track_duration)

        logger.debug(f"{break_left_time=}s")
        logger.debug(f"{playing_duration=}s")

        if break_left_time == 0:
            await asyncio.sleep(1)
            continue

        started = start_playing_track(track_path, playing_duration)
        if not started:
            update_player_status(error_occured=True)
            await asyncio.sleep(15)
            continue

        logger.info(f"Currently playing {track.url}")

        update_player_status(track_url=track.url)
        create_task_to_mark_track_as_played(
            playing_duration,
            track_to_play,
        )
        await asyncio.sleep(1)


def start_music() -> ActionResult:
    global play_loop_task

    updated_stopped_manually = update_player_status(manually_stopped=False)

    if Config.RADIO_CHECK_PLAYBACK:
        try:
            check_stereo_playback()
        except simpleaudio._simpleaudio.SimpleaudioError as ex:
            updated_error_occurence = update_player_status(error_occured=True)
            if not updated_error_occurence:
                logger.error("Music player is still experiencing an error")
            logger.exception(ex)

            return ActionResult(False, updated_error_occurence)
        else:
            update_player_status(error_occured=False)

    if not play_loop_task:
        play_loop_task = asyncio.ensure_future(play_track())
        logger.info("Created playing music task")

    return ActionResult(True, updated_stopped_manually)


@routes.post("/music/start")
async def start_music_endpoint(request: Request) -> Response:
    result = start_music()
    response = {"success": result.success, "byCaller": result.by_caller}
    return web.json_response(response, status=200)


def stop_music() -> ActionResult:
    updated = update_player_status(manually_stopped=True)
    update_player_status(track_url=NoTrackPlaying(""))

    stopped = stop_stream()
    if stopped:
        logger.info("Stopped playing music")
    else:
        logger.warning("Did not stop specific music stream")
    # in case of error in stopping functions
    simpleaudio.stop_all()
    logger.info("Stopped all simpleaudio streams")

    return ActionResult(True, updated)


@routes.post("/music/stop")
async def stop_music_endpoint(request: Request) -> Response:
    result = stop_music()
    response = {"success": result.success, "byCaller": result.by_caller}
    return web.json_response(response, status=200)


@routes.post("/music/fixed")
async def unmark_status_error_occured(request: Request) -> Response:
    updated = update_player_status(error_occured=False)
    response = {"success": True, "byCaller": updated}
    return web.json_response(response, status=200)


@routes.get("/music/health")
async def check_health(request: Request) -> Response:
    return web.json_response({"success": True}, status=200)


async def main() -> None:
    logger.info(f"UTC time: {datetime.utcnow()}")
    logger.warning(
        "Application is starting – "
        + f"host {Config.MUSIC_HOST}, port {Config.MUSIC_PORT}"
    )

    app = web.Application()
    app.add_routes(routes)
    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(
        runner, Config.MUSIC_HOST, Config.MUSIC_PORT, reuse_port=True
    )
    await site.start()
    logger.warning("Application has started")

    start_music()

    assert play_loop_task is not None
    await asyncio.gather(play_loop_task)


if __name__ == "__main__":
    asyncio.run(main())
