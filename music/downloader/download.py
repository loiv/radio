from time import sleep
from typing import Any, Final, Optional

import ffmpeg
import yt_dlp
from shared.config import TrackLibraryStatuses, get_logger
from shared.custom_types import TrackDownloadError, TrackUrl
from shared.database.query_utils import move_waiting_tracks_to_queue
from shared.os_utils import check_path_exists
from yt_dlp.utils import (
    DownloadError,
    GeoRestrictedError,
    UnavailableVideoError,
    YoutubeDLError,
)

logger: Final = get_logger(__name__)


def convert_track(input_fp: str, output_fp: str) -> None:
    in_file = ffmpeg.input(input_fp)
    # https://ffmpeg.org/ffmpeg.html#Audio-Options
    output = ffmpeg.output(
        in_file, output_fp, format="wav", af="loudnorm", ac=2, ar=48000
    )
    ffmpeg.run(output)


def download_track(track_url: TrackUrl, output_fp: str) -> None:
    ydl_opts = {
        "outtmpl": output_fp,
        "format": "bestaudio/best",
    }
    logger.info(f"Pobiorę utwór {track_url} z opcjami {ydl_opts}")
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        try:
            ydl.download([track_url])
        except (DownloadError, GeoRestrictedError, UnavailableVideoError) as e:
            raise TrackDownloadError(f"{e.exc_info[0]} {e.exc_info[1]}")
        except YoutubeDLError as e:
            raise TrackDownloadError(
                f"Generic {e.exc_info[0]} {e.exc_info[1]}"
            )


def find_track_to_download() -> Optional[dict[str, Any]]:
    from shared.database.connection import library

    status_to_search: str = TrackLibraryStatuses.ACCEPTED.value
    track_to_download = library.find_one(
        {
            "status": status_to_search,
            "downloaded": False,
            "needsManualDownload": {"$exists": False},
        }
    )
    if isinstance(track_to_download, dict):
        return track_to_download
    return None


def prepare_music_file(track_url: TrackUrl) -> int:
    from shared.track import Track

    track = Track.match_provider(track_url)
    downloaded_file_path, converted_file_path = track.get_paths()

    if not check_path_exists(downloaded_file_path):
        logger.info(f"Rozpoczynam pobieranie utworu {track.url}")
        try:
            download_track(track.url, downloaded_file_path)
        except TrackDownloadError as e:
            track.mark_manual_download_needed()
            logger.error(f"{e}. Oznaczono utwór do pobrania manualnego")
            return 0
        logger.info(f"Zakończono pobieranie utworu {track.url}")
    else:
        logger.info(f"Utwór {track} został już wcześniej pobrany")

    if not check_path_exists(converted_file_path):
        logger.info(f"Rozpoczęto konwertowanie utworu {track.url}")
        convert_track(downloaded_file_path, converted_file_path)
        logger.info(f"Zakończono konwertowanie utworu {track.url}")
    else:
        logger.info(f"Utwór {track} został już wcześniej skonwertowany")

    updated_count = track.mark_as_downloaded()
    if updated_count == 1:
        logger.info(f"Oznaczono utwór {track} jako pobrany")
    else:
        logger.warning(f"Nie udało się oznaczyć utworu {track} jako pobranego")

    move_waiting_tracks_to_queue(track.url)
    return updated_count


def download_manager() -> None:
    while True:
        track_to_download = find_track_to_download()
        if not track_to_download:
            sleep(5)
            continue

        track_url = track_to_download["trackUrl"]
        updated_count = prepare_music_file(track_url)

        if updated_count == 0:
            logger.warning(f"Nie oznaczono utworu {track_url} jako pobranego")
            sleep(15)
            continue


def main():
    logger.info("Rozpoczęto pobieranie muzyki")
    download_manager()


if __name__ == "__main__":
    main()
