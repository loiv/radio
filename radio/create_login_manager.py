from flask import jsonify
from flask.wrappers import Response
from flask_login import LoginManager

login_manager = LoginManager()


@login_manager.unauthorized_handler
def unauthorized() -> tuple[Response, int]:
    unauthorized_response = {
        "success": False,
        "message": "Wymagane uwierzytelnienie",
    }
    return jsonify(unauthorized_response), 401
