from datetime import datetime
from json import JSONDecodeError
from typing import Any, Final
from typing import Optional as TypingOptional

import requests
from requests.exceptions import Timeout
from shared.config import Config, get_logger
from shared.database.values import get_breaks
from shared.track import Track, TrackIdentifierError
from wtforms import (
    BooleanField,
    Form,
    IntegerField,
    StringField,
    ValidationError,
)
from wtforms.validators import InputRequired, NumberRange, Optional

# https://github.com/wtforms/wtforms/issues/205
# https://github.com/wtforms/wtforms/pull/601
# allow to define HTML field `name` - WTForms 3.0 required

# https://stackoverflow.com/a/37911979
# labels' names doesn't matter when no using rendering

logger: Final = get_logger(__name__)


class LiberalBooleanField(BooleanField):
    def process_formdata(self, valuelist: list[Any]) -> None:
        if not valuelist:
            self.data = None
            return
        value = str(valuelist[0]).lower()
        if value in Config.FALSY_VALUES:
            self.data = False
        elif value in Config.TRUTHY_VALUES:
            self.data = True
        else:
            raise ValidationError(
                "Invalid boolean field value - "
                + f"choose falsy from {set(Config.FALSY_VALUES)} "
                + f"or truthy from {set(Config.TRUTHY_VALUES)}"
            )


class TracksListForm(Form):
    offset = IntegerField(
        name="offset", validators=[Optional(), NumberRange(min=0)]
    )
    show_accepted = LiberalBooleanField(
        name="show-accepted", validators=[Optional()]
    )
    show_pending_approve = LiberalBooleanField(
        name="show-pending-approve", validators=[Optional()]
    )
    show_rejected = LiberalBooleanField(
        name="show-rejected", validators=[Optional()]
    )
    downloaded = LiberalBooleanField(
        name="downloaded", validators=[Optional()]
    )
    search = StringField(
        name="search", validators=[Optional(strip_whitespace=True)]
    )


class UrlCheckForm(Form):
    track_url = StringField(name="track-url", validators=[InputRequired()])

    def validate_track_url(self, field: StringField) -> None:
        try:
            Track.match_provider(field.data)
        except TrackIdentifierError as ex:
            raise ValidationError(str(ex))


def validate_form_datetime(field_data: str) -> datetime:
    if isinstance(field_data, bytes):
        field_data = field_data.decode()
    try:
        parsed_datetime = datetime.fromisoformat(field_data)
    except ValueError:
        raise ValidationError(
            "Nieprawidłowy format daty lub czasu odtwarzania utworu"
        )
    if parsed_datetime.tzinfo is None:
        raise ValidationError("Nie sprecyzowano strefy czasowej czasu")

    requested_break_time_in_local = parsed_datetime.astimezone(
        Config.OUR_TIMEZONE
    ).time()
    if requested_break_time_in_local not in get_breaks():
        raise ValidationError(
            "Podany czas publikacji nie jest początkiem przerwy"
        )
    return parsed_datetime


class PlayDateCheckForm(Form):
    play_date = StringField(name="play-date", validators=[InputRequired()])

    def validate_play_date(self, field: StringField) -> None:
        field_data = field.data
        if isinstance(field_data, bytes):
            field_data = field_data.decode()
        try:
            parsed_date = datetime.fromisoformat(field_data)
        except ValueError:
            raise ValidationError(
                "Nieprawidłowy format daty odtwarzania utworu"
            )
        else:
            self.play_date.parsed = parsed_date


class QueuedTrackCheckForm(UrlCheckForm):
    queued_datetime = StringField(
        name="queued-datetime", validators=[InputRequired()]
    )

    def validate_queued_datetime(self, field: StringField) -> None:
        parsed = validate_form_datetime(field.data)
        field.parsed = parsed


def request_captcha_validation(
    solution: str,
) -> TypingOptional[dict[str, str]]:
    try:
        resp = requests.post(
            "https://api.friendlycaptcha.com/api/v1/siteverify",
            data={
                "solution": solution,
                "secret": Config.CAPTCHA_SECRET_KEY,
                "sitekey": Config.CAPTCHA_SITE_KEY,
            },
            timeout=(1, 5),
        )
    except Timeout:
        logger.warning(
            "Timeout occured during request to Friendly Captcha API"
        )
        return None

    try:
        response_data = resp.json()
    except JSONDecodeError:
        logger.warning("Malformed JSON from Friendly Captcha API")
        return None
    else:
        if (
            not isinstance(response_data, dict)
            or "success" not in response_data
        ):
            return None
        return response_data


class AddTrackQueueForm(UrlCheckForm):
    play_datetime = StringField(
        name="play-datetime", validators=[InputRequired()]
    )

    captcha = StringField(name="frc-captcha-solution", validators=[Optional()])

    def validate_play_datetime(self, field: StringField) -> None:
        parsed = validate_form_datetime(field.data)
        field.parsed = parsed

    def validate_captcha(self, field: StringField) -> None:
        response = request_captcha_validation(field.data)
        if response is None:
            raise ValidationError("Nieznany błąd weryfikacji Captcha")
        success = response["success"]
        if not success:
            raise ValidationError("Nieprawidłowo rozwiązana Captcha")
