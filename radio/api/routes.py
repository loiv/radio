from datetime import datetime

from flask import jsonify
from flask.wrappers import Response
from flask_login import current_user, login_required
from radio.api import bp
from radio.api.forms import (
    AddTrackQueueForm,
    PlayDateCheckForm,
    QueuedTrackCheckForm,
    TracksListForm,
    UrlCheckForm,
)
from radio.request_utils import (
    ErrorResponses,
    WaitResponses,
    no_success_caused_by,
    should_wait_due_to,
    validate_sent_data,
)
from shared.config import Config, TrackLibraryStatuses, get_logger
from shared.custom_types import (
    TrackAPIConfigurationError,
    TrackAPIConnectionError,
    TrackAPIMalformedResponse,
    TrackAPINoResults,
    TrackAPITimeout,
    TrackIdentifierError,
    TrackLibraryUnknownStatus,
    TrackStatusUpdateError,
)
from shared.database.connection import history as tracks_history
from shared.database.connection import queue as tracks_queue
from shared.database.query_utils import (
    get_break_queued_tracks_count,
    get_left_playing_time_during_break,
    get_track_queue_or_history_search_query,
    get_tracks_list,
    remove_rejected_tracks_from_queue,
)
from shared.database.values import get_breaks
from shared.time_utils import check_publication_time_is_over
from shared.track import Track

logger = get_logger(__name__)


@bp.route("/breaks", methods=["GET"])
@bp.route("/breaks/list", methods=["GET"])
def breaks_list() -> tuple[Response, int]:
    resp = {}
    breaks = get_breaks()
    for break_start_time in breaks:
        key = break_start_time.strftime("%H:%M")
        resp[key] = breaks[break_start_time]
    return jsonify(resp), 200


@bp.route("/history", methods=["GET"])
@bp.route("/history/list", methods=["GET"])
@validate_sent_data(PlayDateCheckForm)
def history_list(validated_form: PlayDateCheckForm) -> tuple[Response, int]:
    play_date = validated_form.play_date.parsed

    search_query = get_track_queue_or_history_search_query(play_date)
    cursor = tracks_history.aggregate(search_query)
    response = list(cursor)
    return jsonify(response), 200


@bp.route("/queue", methods=["GET"])
@bp.route("/queue/list", methods=["GET"])
@validate_sent_data(PlayDateCheckForm)
def queue_list(validated_form: PlayDateCheckForm) -> tuple[Response, int]:
    play_date = validated_form.play_date.parsed

    search_query = get_track_queue_or_history_search_query(play_date)
    cursor = tracks_queue.aggregate(search_query)
    queued_tracks = list(cursor)
    return jsonify(queued_tracks), 200


@bp.route("/queue/track", methods=["DELETE"])
@login_required
@validate_sent_data(QueuedTrackCheckForm)
def queue_delete_track(
    validated_form: QueuedTrackCheckForm,
) -> tuple[Response, int]:
    track = Track.match_provider(validated_form.track_url.data)
    playing_datetime = validated_form.queued_datetime.parsed

    deleted_count = track.delete_from_queue_at(playing_datetime)
    if deleted_count > 0:
        logger.info(
            f"Deleted {deleted_count} track{'s' if deleted_count != 1 else ''}"
            + f" {track} from queue at {playing_datetime} on request"
        )
    return jsonify({}), 204


@bp.route("/queue/time-left", methods=["GET"])
@validate_sent_data(PlayDateCheckForm)
def queue_left_time_on_breaks(
    validated_form: PlayDateCheckForm,
) -> tuple[Response, int]:
    resp = {}
    breaks = get_breaks()
    play_datetime: datetime = validated_form.play_date.parsed
    for break_start_time in breaks:
        playing_datetime = datetime.combine(
            play_datetime.date(), break_start_time, tzinfo=Config.OUR_TIMEZONE
        )
        left_time = get_left_playing_time_during_break(playing_datetime)
        key = break_start_time.strftime("%H:%M")
        resp[key] = left_time

    return jsonify(resp), 200


@bp.route("/tracks", methods=["GET"])
@bp.route("/tracks/list", methods=["GET"])
@validate_sent_data(TracksListForm)
def tracks_list(form: TracksListForm) -> tuple[Response, int]:
    statuses: list[str] = []
    if form.show_accepted.data is None or form.show_accepted.data is True:
        statuses.append(TrackLibraryStatuses.ACCEPTED.value)
    if form.show_pending_approve.data is True:
        statuses.append(TrackLibraryStatuses.PENDING_APPROVAL.value)
    if form.show_rejected.data is True and current_user.is_authenticated:
        statuses.append(TrackLibraryStatuses.REJECTED.value)

    offset = form.offset.data or 0
    search = form.search.data
    downloaded = form.downloaded.data

    response = get_tracks_list(statuses, offset, downloaded, search)
    return jsonify(response), 200


@bp.route("/tracks/reject", methods=["PATCH"])
@login_required
@validate_sent_data(UrlCheckForm)
def track_reject(validated_form: UrlCheckForm) -> tuple[Response, int]:
    track_url = validated_form.track_url.data
    track = Track.match_provider(track_url)

    try:
        track.update_status(TrackLibraryStatuses.REJECTED)
    except TrackStatusUpdateError:
        return (
            jsonify(
                {
                    "success": False,
                    "message": "Wystąpił błąd z odrzuceniem utworu",
                }
            ),
            500,
        )

    deleted_count = remove_rejected_tracks_from_queue(track_url)
    logger.info(
        f"Deleted {deleted_count} track{'s' if deleted_count != 1 else ''}"
        + f" {track} from queue as the track had been rejected"
    )
    return jsonify({}), 204


@bp.route("/tracks/accept", methods=["PATCH"])
@login_required
@validate_sent_data(UrlCheckForm)
def track_accept(validated_form: UrlCheckForm) -> tuple[Response, int]:
    track_url = validated_form.track_url.data
    track = Track.match_provider(track_url)

    try:
        track.update_status(TrackLibraryStatuses.ACCEPTED)
    except TrackStatusUpdateError:
        return (
            jsonify(
                {
                    "success": False,
                    "message": "Wystąpił błąd z akceptacją utworu",
                }
            ),
            500,
        )

    return jsonify({}), 204


@bp.route("/tracks/add", methods=["POST"])
@validate_sent_data(AddTrackQueueForm)
def track_add(  # noqa: C901
    validated_form: AddTrackQueueForm,
) -> tuple[Response, int]:
    form = validated_form
    track_url_from_user = form.track_url.data
    play_datetime: datetime = form.play_datetime.parsed

    try:
        track = Track.match_provider(track_url_from_user)
    except TrackIdentifierError:
        return (
            no_success_caused_by(ErrorResponses.TRACK_URL_INVALID),
            403,
        )

    if check_publication_time_is_over(play_datetime):
        logger.debug(f"{play_datetime} is over. Returned 403 error")
        return (
            no_success_caused_by(ErrorResponses.PUBLISH_TIME_PASSED),
            200,
        )
    if play_datetime.isoweekday() > 5:
        return (
            no_success_caused_by(ErrorResponses.DATETIME_DURING_WEEKEND),
            200,
        )
    try:
        track_duration_valid = track.check_valid_duration()
    except (
        TrackAPITimeout,
        TrackAPIMalformedResponse,
        TrackAPIConfigurationError,
        TrackAPINoResults,
    ):
        return (
            no_success_caused_by(ErrorResponses.TRACK_DURATION_API_ERROR),
            500,
        )
    if not track_duration_valid:
        return (
            no_success_caused_by(ErrorResponses.TRACK_TOO_LONG),
            200,
        )

    if (
        get_break_queued_tracks_count(play_datetime) + 1
        > Config.MAX_TRACKS_QUEUED_ONE_BREAK
    ):
        return (
            no_success_caused_by(ErrorResponses.MAX_TRACK_QUEUED_EXCEED),
            200,
        )
    if not get_left_playing_time_during_break(play_datetime):
        return (
            no_success_caused_by(ErrorResponses.BREAK_TIME_EXCEED),
            200,
        )
    try:
        track_status = track.status
    except TrackLibraryUnknownStatus:
        return (
            no_success_caused_by(ErrorResponses.TRACK_DATABASE_BAD_STATE),
            500,
        )

    if track.check_played_or_queued_at(play_datetime):
        return (
            no_success_caused_by(ErrorResponses.TRACK_QUEUED_FOR_THAT_DAY),
            200,
        )

    if track_status == TrackLibraryStatuses.REJECTED:
        return (
            no_success_caused_by(ErrorResponses.TRACK_ALREADY_REJECTED),
            200,
        )
    elif track_status == TrackLibraryStatuses.PENDING_APPROVAL:
        if not track.check_waiting_at(play_datetime):
            track.add_as_waiting_with_datetime_to_play(play_datetime)
            return (
                should_wait_due_to(
                    WaitResponses.TRACK_ALREADY_PENDING_APPROVAL,
                ),
                202,
            )
        else:
            return (
                no_success_caused_by(
                    ErrorResponses.TRACK_POSSIBLY_SCHEDULED_THAT_DAY
                ),
                200,
            )

    elif track_status == TrackLibraryStatuses.ACCEPTED and track.is_downloaded:
        track.add_to_queue_at(play_datetime)
        return jsonify({"success": True, "message": "Dodano do kolejki"}), 201

    elif track_status == TrackLibraryStatuses.ACCEPTED:
        if not track.check_waiting_at(play_datetime):
            track.add_as_waiting_with_datetime_to_play(play_datetime)
            return (
                should_wait_due_to(WaitResponses.TRACK_ALREADY_DOWNLOADING),
                202,
            )
        else:
            return (
                no_success_caused_by(
                    ErrorResponses.TRACK_POSSIBLY_SCHEDULED_THAT_DAY
                ),
                200,
            )
    else:
        try:
            track.add_to_library_as_pending_to_approve()
        except (TrackAPIConnectionError, TrackAPITimeout) as e:
            logger.error(f"{e}. Returned HTTP 500 error to the user")
            return (
                no_success_caused_by(ErrorResponses.TRACK_API_CONNECT_ERROR),
                500,
            )
        except (TrackAPIMalformedResponse, TrackAPINoResults) as e:
            logger.error(f"{e}. Returned HTTP 500 error to the user")
            return (
                no_success_caused_by(ErrorResponses.TRACK_API_BAD_RESPONSE),
                500,
            )
        if not track.check_waiting_at(play_datetime):
            track.add_as_waiting_with_datetime_to_play(play_datetime)
            return (
                should_wait_due_to(
                    WaitResponses.TRACK_ALREADY_PENDING_APPROVAL,
                ),
                202,
            )
        else:
            return (
                no_success_caused_by(
                    ErrorResponses.TRACK_POSSIBLY_SCHEDULED_THAT_DAY
                ),
                200,
            )
