from flask import Blueprint

bp = Blueprint("api", __name__)

from radio.api import routes  # noqa
