from radio.auth.models import User
from wtforms import Form, StringField, ValidationError
from wtforms.validators import InputRequired, Optional


class DeleteAdministratorForm(Form):
    email = StringField(label=None, validators=[InputRequired()])


class AddAdministratorForm(DeleteAdministratorForm):
    identificator = StringField(label=None, validators=[InputRequired()])
    name = StringField(label=None, validators=[InputRequired()])
    profile_picture_url = StringField(label=None, validators=[Optional()])

    def validate_identificator(self, field: StringField) -> None:
        if not User.check_identificator_valid(field.data):
            raise ValidationError("Invalid user identificator (its length)")
