from flask import Blueprint

bp = Blueprint("accounts", __name__)

from radio.accounts import routes  # noqa
