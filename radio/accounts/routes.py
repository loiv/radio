from flask import jsonify
from flask.wrappers import Response
from flask_login import login_required
from radio.accounts import bp
from radio.accounts.forms import AddAdministratorForm, DeleteAdministratorForm
from radio.auth.models import User
from radio.request_utils import validate_sent_data
from shared.config import get_logger
from shared.database.connection import users

logger = get_logger(__name__)


@bp.route("/", methods=["GET"])
@login_required
def list_admin_accounts() -> tuple[Response, int]:
    accounts = list(users.find({}, {"_id": 0}))
    return jsonify(accounts), 200


@bp.route("/", methods=["POST"])
@login_required
@validate_sent_data(AddAdministratorForm)
def add_admin_account(
    validated_form: AddAdministratorForm,
) -> tuple[Response, int]:
    user = User(
        validated_form.identificator.data,
        validated_form.name.data,
        validated_form.email.data,
        validated_form.profile_picture_url.data,
    )
    if user.check_in_database():
        return jsonify(), 409

    user.save_to_database()
    return jsonify(), 204


@bp.route("/", methods=["DELETE"])
@login_required
@validate_sent_data(DeleteAdministratorForm)
def delete_admin_account(
    validated_form: DeleteAdministratorForm,
) -> tuple[Response, int]:
    account_email = validated_form.email.data
    users.delete_one({"email": account_email})
    return jsonify(), 204
