from flask import jsonify
from flask.wrappers import Response
from radio.main import bp


@bp.route("/health")
def check_health() -> tuple[Response, int]:
    return jsonify({"success": True}), 200
