from flask import Blueprint

bp = Blueprint("main", __name__)

from radio.main import routes  # noqa
