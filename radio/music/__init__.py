from flask import Blueprint

bp = Blueprint("music", __name__)

from radio.music import routes  # noqa
