import logging
from json import JSONDecodeError
from typing import Literal
from urllib.parse import urljoin

from flask import jsonify
from flask.wrappers import Response
from flask_login import login_required
from radio.music import bp
from shared.config import Config

logger = logging.getLogger(__name__)


def _get_music_player_url(action: str) -> str:
    return urljoin(
        f"http://{Config.MUSIC_HOST}:{Config.MUSIC_PORT}/music/", action
    )


def make_music_action(
    action: str, verb: Literal["post", "get"]
) -> tuple[Response, int]:
    import requests

    url = _get_music_player_url(action)

    try:
        music_response = getattr(requests, verb)(url, timeout=(1, 3))
    except requests.exceptions.ConnectionError:
        message = "Cannot connect to service"
        logger.error(f"{message} music at {url}")
        return jsonify({"success": False, "message": message}), 503
    except requests.exceptions.Timeout:
        message = "Timeout occured during POST request"
        logger.error(f"{message} to {url}")
        return jsonify({"success": False, "message": message}), 504

    try:
        decoded_response = music_response.json()
    except JSONDecodeError:
        message = "Malformed JSON music server response"
        logger.error(f"{message}: {music_response.text}")
        return jsonify({"success": False, "message": message}), 502
    return jsonify(decoded_response), music_response.status_code


@bp.route("/stop", methods=["POST"])
@login_required
def music_stop() -> tuple[Response, int]:
    return make_music_action("stop", "post")


@bp.route("/start", methods=["POST"])
@login_required
def music_start() -> tuple[Response, int]:
    return make_music_action("start", "post")


@bp.route("/fixed", methods=["POST"])
@login_required
def music_mark_as_fixed() -> tuple[Response, int]:
    return make_music_action("fixed", "post")


@bp.route("/player/health")
def check_music_player_health() -> tuple[Response, int]:
    return make_music_action("health", "get")
