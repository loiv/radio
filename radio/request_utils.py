from enum import Enum
from functools import wraps
from typing import Any, Callable, Concatenate, Optional, ParamSpec, TypeVar

from flask import jsonify, request
from flask.wrappers import Request, Response
from werkzeug.exceptions import BadRequest
from wtforms.form import Form


# Custom class to wrap `dict` and to implement `getlist` method in order to
# https://github.com/wtforms/wtforms/blob/
# 1184944a6d5e6368e9d5e1424b6955846dd1e69f/src/wtforms/meta.py#L41
class MyMultiDict(dict):
    def getlist(self, key: str) -> list[Any]:
        value = self[key]
        if not isinstance(value, list):
            value = [value]
        return value

    def getall(self, key: str) -> list[Any]:
        return [self[key]]


class WaitResponses(Enum):
    TRACK_ALREADY_PENDING_APPROVAL = "Utwór oczekuje na akceptację"
    TRACK_ALREADY_DOWNLOADING = "Utwór jest w trakcie pobierania"


class ErrorResponses(Enum):
    PUBLISH_TIME_PASSED = "Podany czas publikacji już minął"
    BREAK_TIME_EXCEED = "Przekroczono czas trwania przerwy"
    DATETIME_DURING_WEEKEND = "Podany czas jest w trakcie weekendu"
    MAX_TRACK_QUEUED_EXCEED = (
        "Przekroczono maksymalną liczbę utworów na przerwie"
    )
    TRACK_ALREADY_REJECTED = "Utwór jest odrzucony"
    TRACK_QUEUED_FOR_THAT_DAY = (
        "Utwór znajduje się na liście odtwarzania tego dnia"
    )
    TRACK_DURATION_API_ERROR = (
        "Wystąpił błąd podczas pobierania informacji o długości utworu"
    )
    TRACK_API_CONNECT_ERROR = (
        "Wystąpił błąd podczas łączenia się z dostawcą utworów"
    )
    TRACK_API_BAD_RESPONSE = (
        "Otrzymano nieprawidłową odpowiedź od dostawcy utworów"
    )
    TRACK_TOO_LONG = "Utwór jest zbyt długi"
    TRACK_URL_INVALID = "Nieprawidłowy adres utworu"
    TRACK_DATABASE_BAD_STATE = "Utwór ma nieznany status w bazie danych"
    TRACK_POSSIBLY_SCHEDULED_THAT_DAY = (
        "Utwór być może odtworzy się już tego dnia"
    )


def make_error_response_bad_params(
    form_errors: dict[str, list[str]]
) -> Response:
    return jsonify(
        {
            "success": False,
            "message": "Błędne parametry",
            "description": form_errors,
        }
    )


def no_success_caused_by(message: ErrorResponses) -> Response:
    return jsonify(
        {
            "success": False,
            "message": message.value,
        }
    )


def should_wait_due_to(
    message: WaitResponses,
) -> Response:
    return jsonify(
        {
            "success": True,
            "message": message.value,
            "description": "Musisz poczekać",
        }
    )


def check_valid_json_data(
    request: Request,
) -> tuple[Optional[dict[str, Any]], Optional[dict[str, str | bool]]]:
    try:
        decoded = request.get_json()
    except BadRequest:
        pass
    else:
        return (decoded, None)

    error_http_message: dict[str, str | bool] = {
        "success": False,
        "message": "Błędny typ, format lub składnia danych",
        "description": "Endpoint przyjmuje JSON",
    }
    return (
        None,
        error_http_message,
    )


F = TypeVar("F", bound=Form)
P = ParamSpec("P")

RespSC = tuple[Response, int]


def validate_sent_data(
    validation_form: type[F],
) -> Callable[[Callable[Concatenate[F, P], RespSC]], Callable[P, RespSC]]:
    def decorated(
        f: Callable[Concatenate[F, P], RespSC]
    ) -> Callable[P, RespSC]:
        @wraps(f)
        def inner(*args: P.args, **kwargs: P.kwargs) -> RespSC:
            if request.method == "GET":
                form = validation_form(request.args)
            else:
                valid_json_data, error_http_message = check_valid_json_data(
                    request
                )
                if error_http_message or valid_json_data is None:
                    return jsonify(error_http_message), 400

                multi_dict = MyMultiDict(valid_json_data)
                form = validation_form(multi_dict)

            if not form.validate():
                form_errors = {}
                field_code_name: str
                for field_code_name in form.data:
                    # match fields errors keys names from a request
                    form_field = getattr(form, field_code_name)
                    field_error = form.errors.get(field_code_name)
                    if field_error:
                        form_errors[form_field.name] = field_error

                return make_error_response_bad_params(form_errors), 422
            return f(form, *args, **kwargs)

        return inner

    return decorated
