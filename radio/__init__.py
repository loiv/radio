from shared.config import Config


def create_app(config_class=Config):
    from flask import Flask

    app = Flask(__name__)
    app.config.from_object(config_class)

    from radio.create_login_manager import login_manager

    login_manager.init_app(app)
    from radio.api import bp as api_bp

    app.register_blueprint(api_bp, url_prefix="/api/v1")

    from radio.auth import bp as auth_bp

    app.register_blueprint(auth_bp, url_prefix="/auth")

    from radio.accounts import bp as accounts_bp

    app.register_blueprint(accounts_bp, url_prefix="/accounts")

    from radio.music import bp as music_bp

    app.register_blueprint(music_bp, url_prefix="/music")

    from radio.main import bp as main_bp

    app.register_blueprint(main_bp, url_prefix="/")

    return app
