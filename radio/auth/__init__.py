from flask import Blueprint

bp = Blueprint("auth", __name__)

from radio.auth import routes  # noqa
