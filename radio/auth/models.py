from __future__ import annotations

from typing import Optional, Union

from flask_login import UserMixin
from radio.create_login_manager import login_manager


class User(UserMixin):
    def __init__(
        self,
        identificator: str,
        name: str,
        email: str,
        profile_picture_url: Optional[str] = None,
    ):
        if not self.check_identificator_valid(identificator):
            raise ValueError("Incorrect identificator's length")
        self.identificator = identificator
        self.name = name
        self.email = email
        self.profile_picture_url = profile_picture_url

    @staticmethod
    def check_identificator_valid(identificator: str):
        return len(str(identificator)) == 21

    @staticmethod
    def load_user_data_from_database(
        identificator: Union[int, str]
    ) -> Optional[User]:
        from shared.database.connection import users as db_users

        user_data = db_users.find_one({"identificator": str(identificator)})
        if not user_data:
            return None
        user = User(
            identificator=user_data["identificator"],
            name=user_data["name"],
            email=user_data["email"],
            profile_picture_url=user_data.get("profile_picture_url"),
        )
        return user

    def save_to_database(self) -> None:
        from shared.database.connection import users

        user_data = {
            "identificator": str(self.identificator),
            "name": self.name,
            "email": self.email,
        }
        user_profile_picture_url = self.profile_picture_url
        if user_profile_picture_url is not None:
            user_data["profile_picture_url"] = user_profile_picture_url
        users.insert_one(user_data)

    def check_in_database(self) -> bool:
        from shared.database.connection import users

        return bool(
            users.count_documents({"identificator": self.identificator})
        )

    def get_id(self) -> int:
        return int(self.identificator)

    def __repr__(self) -> str:
        return f"<User {self.name}>"


@login_manager.user_loader
def load_user(user_id: Union[str, int]) -> Optional[User]:
    return User.load_user_data_from_database(user_id)
