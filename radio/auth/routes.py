from flask import jsonify
from flask.wrappers import Response
from flask_login import login_required, login_user
from radio.auth import bp
from radio.auth.forms import GoogleSignInTokenSubmitForm
from radio.auth.models import User
from radio.request_utils import validate_sent_data
from shared.config import get_logger

logger = get_logger(__name__)


@bp.route("/token", methods=["POST"])
@validate_sent_data(GoogleSignInTokenSubmitForm)
def check_google_sign_in_token(
    validated_form: GoogleSignInTokenSubmitForm,
) -> tuple[Response, int]:
    user_jwt_token = validated_form.credential.parsed_dict
    user_id = user_jwt_token["sub"]
    user = User.load_user_data_from_database(user_id)
    if user is None:
        return (
            jsonify(
                {
                    "success": False,
                    "message": "No user with that identificator exists",
                }
            ),
            401,
        )
    login_user(user)
    return jsonify({"success": True, "message": "Successfully signed in"}), 200


@bp.route("/check")
@login_required
def check_admin_authenticated() -> tuple[Response, int]:
    return (
        jsonify(
            {
                "success": True,
                "message": "You are authenticated to manage tracks",
            }
        ),
        200,
    )
