import json
import re
from datetime import datetime, timedelta

import jwt
import requests
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat
from cryptography.x509 import load_pem_x509_certificate
from jwt.exceptions import (  # type: ignore
    DecodeError,
    ExpiredSignatureError,
    ImmatureSignatureError,
    InvalidAlgorithmError,
    InvalidAudienceError,
    InvalidIssuedAtError,
    InvalidKeyError,
    InvalidSignatureError,
    InvalidTokenError,
    MissingRequiredClaimError,
)
from requests.exceptions import Timeout
from shared.config import get_logger
from wtforms import Form, StringField, ValidationError
from wtforms.validators import InputRequired, Optional

logger = get_logger(__name__)

max_age_header_pattern = re.compile(r"(?<=max-age=)\d+")


def _save_google_certs_to_db(fetched_certs: dict, max_age: int):
    from shared.database.connection import certs as db_certs

    expire_at = datetime.utcnow() + timedelta(seconds=max_age - 5)
    certs_to_save = []
    for kid in fetched_certs:
        cert = {
            "format": "PEM",
            "owner": "Google",
            "kid": kid,
            "cert": fetched_certs[kid],
            "expiresAt": expire_at,
        }
        certs_to_save.append(cert)
    db_certs.insert_many(certs_to_save)


def _get_google_certs_from_db() -> dict[str, str]:
    from shared.database.connection import certs

    certs_list = list(certs.find({"format": "PEM", "owner": "Google"}))
    certs = {}
    for cert in certs_list:
        certs[cert["kid"]] = cert["cert"]
    return certs


def get_gsi_cert_list() -> dict[str, str]:
    if certs_from_db := _get_google_certs_from_db():
        return certs_from_db

    unable_fetch_cert_list_msg = (
        "Nie udało się pobrać listy certyfikatów do"
        + " zweryfikowania podpisu tokenu JWT"
    )
    try:
        response = requests.get(
            "https://www.googleapis.com/oauth2/v1/certs", timeout=(1, 3)
        )
    except Timeout:
        raise ValidationError(f"{unable_fetch_cert_list_msg} - timeout")

    try:
        fetched_certs: dict[str, str] = response.json()
    except json.JSONDecodeError:
        raise ValidationError(
            f"{unable_fetch_cert_list_msg} - błędny format odpowiedzi serwera"
        )

    cache_control_header = response.headers["cache-control"]
    searched = max_age_header_pattern.search(cache_control_header)
    if searched is not None:
        max_age_str = searched.group(0)
        try:
            max_age = int(max_age_str)
        except ValueError:
            logger.warning(
                "Nieprawidłowa wartość max-age nagłówka cache-control: "
                + f"{cache_control_header}"
            )
        else:
            _save_google_certs_to_db(fetched_certs, max_age)
    return fetched_certs


def _get_audience_list() -> list:
    from flask import current_app

    return current_app.config["GSI_CLIENTS_IDS"]


def _get_gsi_accounts_domain() -> str:
    from flask import current_app

    return current_app.config["GSI_ACCOUNTS_DOMAIN"]


class GoogleSignInTokenSubmitForm(Form):
    credential = StringField(label=None, validators=[InputRequired()])
    g_csrf_token = StringField(label=None, validators=[Optional()])

    # https://developers.google.com/identity/gsi/web/guides/verify-google-id-token?hl=en # noqa
    def validate_credential(self, field: StringField) -> None:
        credential_data = field.data
        gsi_certificate_list = get_gsi_cert_list()

        header = jwt.get_unverified_header(credential_data)
        if (
            header
            and isinstance(header, dict)
            and "kid" not in header
            or header["kid"] not in gsi_certificate_list
        ):
            raise ValidationError(
                "Nieprawidłowa wartość ID klucza do weryfikacji podpisu"
            )

        used_cert = header["kid"]
        pem_cert = gsi_certificate_list[used_cert].encode()

        try:
            public_key = load_pem_x509_certificate(pem_cert).public_key()
            serialized_key = public_key.public_bytes(
                Encoding.PEM, PublicFormat.SubjectPublicKeyInfo
            ).decode()
        except Exception:
            raise ValidationError(
                "Nie udało się uzyskać klucza publicznego"
                " z certyfikatu użytego do podpisu tokenu JWT"
            )

        decoding_options = {
            "verify_signature": True,
            "require": [
                "iss",  # verified manually
                "sub",  # we use it as an user identificator
                "aud",  # verified by pyjwt
                "exp",  # verified by pyjwt
                "nbf",  # verified by pyjwt
                "iat",  # verified by pyjwt
                "jti",
                "hd",  # verified manually
                "email",  # we use it as an user email
                "name",  # we use it as an user name
            ],
        }

        try:
            decoded_jwt = jwt.decode(
                credential_data,
                serialized_key,
                algorithms=["RS256"],
                verify_signature=True,
                verify_exp=True,
                verify_nbf=True,
                verify_iat=True,
                audience=_get_audience_list(),
                leeway=0.0,
                options=decoding_options,
            )
        except MissingRequiredClaimError as ex:
            raise ValidationError(
                "Token nie zawiera wszystkich wymaganych danych - "
                + f"pola {ex.claim}"
            )
        except (
            ImmatureSignatureError,
            InvalidIssuedAtError,
            ExpiredSignatureError,
        ):
            raise ValidationError("Nieprawidłowy czas podpisu tokenu")

        except InvalidAlgorithmError:
            raise ValidationError("Nieznany algorytm podpisu tokenu")
        except InvalidKeyError:
            raise ValidationError("Niepoprawny format klucza podpisu tokenu")
        except InvalidAudienceError:
            raise ValidationError("Niepoprawny odbiorca (audience) tokenu")

        except (InvalidTokenError, DecodeError, InvalidSignatureError):
            raise ValidationError("Wystąpił błąd podczas dekodowania tokenu")

        else:
            if decoded_jwt["iss"] not in [
                "accounts.google.com",
                "https://accounts.google.com",
            ]:
                raise ValidationError(
                    "Wystawca (issuer) tokenu JWT jest nieprawidłowy"
                )
            domain = _get_gsi_accounts_domain()
            if domain and decoded_jwt["hd"] != domain:
                raise ValidationError("Niepoprawna domena konta")

            self.credential.parsed_dict = decoded_jwt
